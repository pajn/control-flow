import {Editor} from '@pajn/control-flow/lib/board/components/editor'
import {load, save} from '@pajn/control-flow/lib/board/lib/actions/editor'
import {
  EditorState,
  editorReducer,
} from '@pajn/control-flow/lib/board/lib/state/editor'
import {nodeTypes as logicNodeTypes} from '@pajn/control-flow/lib/nodes/logic'
import {nodeTypes as mathNodeTypes} from '@pajn/control-flow/lib/nodes/math'
import {ContextMenuProvider} from '@pajn/control-flow/lib/ui/components/context_menu'
import React from 'react'
import {render} from 'react-dom'
import {HotKeys} from 'react-hotkeys'
import {Provider, Store} from 'react-redux'
import {applyMiddleware, createStore} from 'redux'
import thunk from 'redux-thunk'

const nodeTypes = {...mathNodeTypes, ...logicNodeTypes}

const store = createStore(
  editorReducer as any,
  applyMiddleware(thunk),
) as Store<EditorState>

store.dispatch(load(nodeTypes) as any)

const keyMap = {
  save: ['ctrl+s', 'command+s'],
}

const App = () => (
  <Provider store={store}>
    <ContextMenuProvider>
      <HotKeys
        keyMap={keyMap}
        handlers={{
          save: (e?: KeyboardEvent) => {
            if (e) e.preventDefault()
            store.dispatch(save() as any)
          },
        }}
      >
        <Editor nodeTypes={nodeTypes} />
      </HotKeys>
    </ContextMenuProvider>
  </Provider>
)

render(<App />, document.querySelector('#app'))
