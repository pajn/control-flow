export type Point = {x: number; y: number}
export type Rect = [Point, Point]
export type Direction = 'start' | 'end' | 'left' | 'right' | 'top' | 'bottom'

export const offset = (base: number, amount: number) => -base / 2 + amount
export const offsetEnd = (base: number, amount: number) => base / 2 - amount
export const translate = (x: number, y: number) => `translate(${x}, ${y})`
export const translateX = (x: number) => `translate(${x}, 0)`
export const translateY = (y: number) => `translate(0, ${y})`
export const normalizeDir = (dir: Direction) =>
  dir === 'left' || dir === 'top' ? 'start' : 'end'
export const align = (direction: Direction, value: number) =>
  normalizeDir(direction) === 'start' ? value : -value

export const rectPath = ([a, b]: Rect) =>
  `M ${a.x},${a.y} l ${b.x},0 0,${b.y} ${-b.x},0 0,${-b.y}`

export const inRect = (point: Point, rect: Rect) =>
  point.x >= Math.min(rect[0].x, rect[1].x) &&
  point.x <= Math.max(rect[0].x, rect[1].x) &&
  point.y >= Math.min(rect[0].y, rect[1].y) &&
  point.y <= Math.max(rect[0].y, rect[1].y)

export namespace Point {
  export function add(a: Point, b: Point) {
    return {
      x: a.x + b.x,
      y: a.y + b.y,
    }
  }
  export function subtract(a: Point, b: Point) {
    return {
      x: a.x - b.x,
      y: a.y - b.y,
    }
  }
  export function distance(a: Point, b: Point) {
    return Math.sqrt((b.x - a.x) ** 2 + (b.y - a.y) ** 2)
  }
}
