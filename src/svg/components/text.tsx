import React from 'react'
import {ReactChild} from 'react'
import {pure} from 'recompose'

export type TextProps = {
  x?: number
  y?: number
  vertical?: 'center'
  horizontal?: 'center' | 'start' | 'end'

  bold?: boolean
  size?: number

  title?: string

  children?: ReactChild
}

export const TextView = (props: TextProps) =>
  <text
    x={props.x}
    y={props.y}
    textAnchor={props.horizontal === 'center' ? 'middle' : props.horizontal}
    alignmentBaseline={props.vertical === 'center' ? 'central' : undefined}
    fill="white"
    fontSize={props.size}
    fontWeight={props.bold ? 700 : undefined}
    letterSpacing={props.bold ? 1.1 : undefined}
  >
    {props.title &&
      <title>
        {props.title}
      </title>}
    {props.children}
  </text>

export const Text = pure(TextView)
