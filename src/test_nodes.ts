import {NodeType, NodeTypeImplementation} from './schematic-core/entities'

export const nodeTypes: {[kind: string]: NodeType} = {
  '+': {
    id: '+',
    name: '+',
    fullName: 'Add',
    ports: {
      a: {id: 'a', kind: 'ValueInput', dataType: 'Num'},
      b: {id: 'b', kind: 'ValueInput', dataType: 'Num'},
      out: {id: 'out', kind: 'ValueOutput', dataType: 'Num'},
    },
    properties: {},
  },
  lt: {
    id: 'lt',
    name: '<',
    fullName: 'Less Than',
    ports: {
      a: {id: 'a', kind: 'ValueInput', name: 'a', dataType: 'Num'},
      b: {id: 'b', kind: 'ValueInput', name: 'b', dataType: 'Num'},
      out: {id: 'out', kind: 'ValueOutput', dataType: 'Bool'},
    },
    properties: {},
  },
  interval: {
    id: 'interval',
    name: 'Interval',
    ports: {
      event: {id: 'event', kind: 'EventOutput'},
    },
    properties: {
      duration: {
        id: 'duration',
        name: 'duration',
        dataType: 'Num',
        required: true,
      },
    },
  },
  log: {
    id: 'log',
    name: 'Log',
    ports: {
      event: {id: 'event', kind: 'EventInput'},
      value: {id: 'value', kind: 'ValueInput', name: 'value', dataType: 'Num'},
    },
    properties: {},
  },
  numValue: {
    id: 'numValue',
    name: 'Number',
    ports: {
      value: {id: 'value', kind: 'ValueOutput', dataType: 'Num'},
    },
    properties: {
      value: {id: 'value', name: 'value', dataType: 'Num', required: true},
    },
  },
}

export const implementations: {[kind: string]: NodeTypeImplementation} = {
  '+': {
    id: '+',

    getPort(_, ctrl) {
      return ctrl.getPort('a') + ctrl.getPort('b')
    },
  },
  lt: {
    id: 'lt',

    getPort(_, ctrl) {
      return ctrl.getPort('a') < ctrl.getPort('b')
    },
  },
  interval: {
    id: 'interval',

    onStart(ctrl) {
      ctrl.state = {
        intervalId: setInterval(
          () => ctrl.dispatchEvent('event'),
          ctrl.getProperty('duration'),
        ),
      }
    },
    onStop(ctrl) {
      clearInterval(ctrl.state.intervalId)
    },
    onPropertiesChanged(ctrl) {
      this.onStop!(ctrl)
      this.onStart!(ctrl)
    },
  },
  log: {
    id: 'log',

    onEvent: (event, ctrl) => {
      if (event === 'event') {
        console.log(ctrl.getPort('value'))
      }
    },
  },
  numValue: {
    id: 'numValue',

    getPort(id, ctrl) {
      return +ctrl.getProperty(id)
    },
  },
}
