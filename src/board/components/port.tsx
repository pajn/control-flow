import React from 'react'
import {SVGAttributes} from 'react'
import {connect} from 'react-redux'
import {ComponentEnhancer, compose} from 'recompose'
import {action} from 'redux-decorated'
import {NodeTypes, Port} from '../../schematic-core/entities'
import {
  isInput,
  isValuePort,
  portColor,
} from '../../schematic-core/node_helpers'
import {Text} from '../../svg/components/text'
import {Point, align, normalizeDir, translateX} from '../../svg/lib/svg_helpers'
import {Draggable} from '../../ui/components/draggable'
import {
  completeWorkingConnection,
  startCreateConnection,
  startMovingConnection,
} from '../lib/actions/working_connections'
import {EditorState} from '../lib/state/editor'
import {getConnection, isPortConnected} from '../lib/state/program'
import {workingConnectionActions} from '../lib/state/working_connection'

export {ComponentEnhancer as _ComponentEnhancer}

export type PortProps = {
  nodeTypes: NodeTypes
  node: string
  port: Port
  position: Point
  align?: 'left' | 'right'
}

export type PrivatePortProps = PortProps & {
  isConnected: boolean
  debugValue: any
  haveWorkingConnection: boolean
  workingConnectionRelativePosition: Point | null
  startCreateConnection: () => void
  startMovingConnection: () => void
  moveWorkingConnection: (point: Point) => void
  completeWorkingConnection: (e: React.MouseEvent<any>) => void
}

export const enhance = compose<PrivatePortProps, PortProps>(
  connect(
    (
      state: EditorState,
      {node, port}: PortProps,
    ): Partial<PrivatePortProps> => ({
      isConnected: isPortConnected(state.program, node, port.id),
      debugValue: isPortConnected(state.program, node, port.id)
        ? state.debug.values.get(getConnection(state.program, node, port.id))
        : undefined,
      haveWorkingConnection: !!state.workingConnection,
      workingConnectionRelativePosition:
        state.workingConnection && state.workingConnection.relativePosition,
    }),
    (
      dispatch,
      {nodeTypes, node, port}: PortProps,
    ): Partial<PrivatePortProps> => ({
      startCreateConnection: () =>
        dispatch(startCreateConnection(nodeTypes, {node, port: port.id})),
      startMovingConnection: () =>
        dispatch(startMovingConnection(nodeTypes, {node, port: port.id})),
      moveWorkingConnection: position =>
        dispatch(
          action(workingConnectionActions.moveWorkingConnection, {position}),
        ),
      completeWorkingConnection: (e: React.MouseEvent<any>) => {
        e.stopPropagation()
        dispatch(completeWorkingConnection({node, port: port.id}, e.shiftKey))
      },
    }),
  ),
)

export type PortShapeProps = {
  port: Port
  cx: number
  cy: number
  stroke: string
  isConnected: boolean
} & SVGAttributes<any>

export const PortShape = ({
  port,
  cx,
  cy,
  stroke,
  isConnected,
  ...props
}: PortShapeProps) =>
  isValuePort(port) ? (
    <circle
      {...props}
      cx={cx}
      cy={cy}
      r="3"
      stroke={stroke}
      strokeWidth="2"
      fill={isConnected ? stroke : '#666666'}
    />
  ) : (
    <path
      {...props}
      d={`M ${cx},${cy} m -5,-3 l 5,0 3,3 -3,3 -5,0 0,-6`}
      stroke={stroke}
      strokeWidth="2"
      strokeLinecap="square"
      fill={isConnected ? stroke : '#666666'}
      transform={isInput(port) ? translateX(2) : undefined}
    />
  )

export const PortCView = ({
  port,
  position,
  align: dir = 'left',
  isConnected,
  debugValue,
  haveWorkingConnection,
  workingConnectionRelativePosition,
  startCreateConnection,
  startMovingConnection,
  moveWorkingConnection,
  completeWorkingConnection,
}: PrivatePortProps) => (
  <g onMouseUp={haveWorkingConnection ? completeWorkingConnection : undefined}>
    <Draggable
      onStart={() => {
        if (isConnected) {
          if (isValuePort(port)) {
            if (isInput(port)) {
              return startMovingConnection()
            }
          } else {
            if (!isInput(port)) {
              return startMovingConnection()
            }
          }
        }
        startCreateConnection()
      }}
      onMove={moveWorkingConnection}
      startPosition={workingConnectionRelativePosition!}
    >
      {({onMouseDown}) => (
        <PortShape
          onMouseDown={onMouseDown}
          port={port}
          cx={position.x}
          cy={position.y}
          stroke={portColor(port)}
          isConnected={isConnected}
          style={{cursor: 'pointer'}}
        />
      )}
    </Draggable>
    {port.name && (
      <Text
        x={position.x + align(dir, 10)}
        y={position.y + 3}
        horizontal={normalizeDir(dir)}
        vertical="center"
        size={14}
      >
        {port.name}
      </Text>
    )}
    {debugValue && <title>{debugValue}</title>}
  </g>
)

export const PortC = enhance(PortCView)
