import {Set} from 'immutable'
import React from 'react'
import {connect} from 'react-redux'
import {ComponentEnhancer, compose} from 'recompose'
import {action} from 'redux-decorated'
import {Column} from 'styled-material/lib/layout'
import {Runtime} from '../../runtime/entities'
// import {createRuntime} from '../../runtime/runtime'
import {NodeType} from '../../schematic-core/entities'
import {inRect, rectPath} from '../../svg/lib/svg_helpers'
// import {implementations} from '../../test'
import {
  SelectionAreaProvidedProps,
  withSelectionArea,
} from '../../ui/enhancers/selection_area'
// import {createDebugger} from '../lib/state/debug'
import {EditorState, editorActions} from '../lib/state/editor'
import {workingConnectionActions} from '../lib/state/working_connection'
import {Board} from './board'
import {ConnectionC, ConnectionLine} from './connection'
import {NodeC} from './node'

export {ComponentEnhancer as _ComponentEnhancer}

export type ProgramProps = {
  nodeTypes: {[kind: string]: NodeType}
}
export type PrivateProgramProps = ProgramProps &
  SelectionAreaProvidedProps & {
    state: EditorState
    setRuntime: (runtime: Runtime | undefined) => void
    dropWorkingConnection: () => void
    setSelection: (nodes: Array<string>) => void
    clearSelection: () => void
    dispatch: any
  }

export const enhance = compose<PrivateProgramProps, ProgramProps>(
  connect(
    (state: EditorState) => ({state}),
    (dispatch): Partial<PrivateProgramProps> => ({
      setRuntime: runtime =>
        dispatch(action(editorActions.setRuntime, {runtime})),
      dropWorkingConnection: () =>
        dispatch(action(workingConnectionActions.clearWorkingConnection, {})),
      setSelection: nodes =>
        dispatch(action(editorActions.selectNodes, {selection: Set(nodes)})),
      clearSelection: () => dispatch(action(editorActions.clearSelection, {})),
      dispatch,
    }),
  ),
  withSelectionArea<PrivateProgramProps>({
    onSelected: (area, {state: {program}, setSelection}) => {
      const nodes = Object.values(program.graph.nodes)
        .filter(n => inRect(n.position, area))
        .map(n => n.id)

      setSelection(nodes)
    },
    clearSelection: ({clearSelection}) => clearSelection(),
  }),
)

export const ProgramView = ({
  nodeTypes,
  state,
  dropWorkingConnection,
  selectionArea,
  onMouseDown,
  clearSelection,
}: // setRuntime,
// dispatch,
PrivateProgramProps) => (
  <Column style={{height: '100%'}}>
    {/* <Row>
      {state.runtime
        ? <button
            onClick={() => {
              state.runtime!.stop()
              setRuntime(undefined)
            }}
          >
            Stop
          </button>
        : <button
            onClick={() => {
              const runtime = createRuntime(
                {
                  program: state.program,
                  implementations,
                },
                createDebugger(dispatch),
              )
              runtime.start()
              setRuntime(runtime)
            }}
          >
            Start
          </button>}
    </Row> */}
    <Board
      nodeTypes={nodeTypes}
      onMouseDown={onMouseDown}
      onMouseUp={state.workingConnection ? dropWorkingConnection : undefined}
      onClick={clearSelection}
    >
      <g>
        {Object.values(state.program.graph.connections).map(connection => (
          <ConnectionC key={connection.id} connection={connection} />
        ))}
      </g>
      <g>
        {state.workingConnection && (
          <ConnectionLine
            startPosition={
              state.workingConnection.fromInput
                ? state.workingConnection.currentPosition
                : state.workingConnection.startPosition
            }
            endPosition={
              state.workingConnection.fromInput
                ? state.workingConnection.startPosition
                : state.workingConnection.currentPosition
            }
            type="creating"
          />
        )}
      </g>
      <g>
        {Object.values(state.program.graph.nodes).map(node => (
          <NodeC key={node.id} nodeTypes={nodeTypes} id={node.id} />
        ))}
      </g>
      {selectionArea && (
        <path
          d={rectPath(selectionArea)}
          stroke="white"
          strokeWidth="2"
          fill="rgba(255, 255, 255, 0.1)"
        />
      )}
    </Board>
  </Column>
)

export const ProgramC = enhance(ProgramView)
