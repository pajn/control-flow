import React from 'react'
import {connect} from 'react-redux'
import {ComponentEnhancer, compose} from 'recompose'
import {keyframes} from 'styled-components'
import {
  Connection,
  DataType,
  Node,
  Program,
} from '../../schematic-core/entities'
import {
  getNode,
  getType,
  isValuePort,
  typeColors,
} from '../../schematic-core/node_helpers'
import {Point} from '../../svg/lib/svg_helpers'
import {withMemoizedProps} from '../../ui/enhancers/memoize_props'
import {portPosition} from '../lib/measurement'
import {EditorState} from '../lib/state/editor'

export {ComponentEnhancer as _ComponentEnhancer}

const connectionColors = {
  ...typeColors,
  Event: 'white',
  creating: 'rgba(255, 255, 255, 0.7)',
}

const followPath = keyframes`
  100% {
    motion-offset: 100%;
  }
`

const shrink = keyframes`
  0% {
    r: 4;
  }
  70% {
    r: 4;
  }
  100% {
    r: 2;
  }
`

const activeTimes = Array.from({length: 5}) as Array<void>

const Active = ({path}: {path: string}) => (
  <g>
    {activeTimes.map((_, i) => (
      <circle
        key={i}
        fill="white"
        style={{
          motionPath: `path('${path}')`,
          animation: `
            ${followPath} 800ms linear ${(i - 2) * -150}ms infinite both,
            ${shrink} 500ms linear
          `,
        }}
      />
    ))}
  </g>
)

export type ConnectionProps = {
  connection: Connection
}

export type PrivateConnectionProps = ConnectionProps & {
  isActive: boolean
  nodeTypes: Program['nodeTypes']
  startNode: Node
  endNode: Node
  startNodePosition: Point
  endNodePosition: Point
}

export const enhance = compose<PrivateConnectionProps, ConnectionProps>(
  connect(
    (
      state: EditorState,
      {connection}: ConnectionProps,
    ): Partial<PrivateConnectionProps> => ({
      isActive: state.debug.events.get(connection.id),
      nodeTypes: state.program.nodeTypes,
      startNode: getNode(state.program, connection.start.node),
      endNode: getNode(state.program, connection.end.node),
    }),
  ),
  withMemoizedProps<PrivateConnectionProps>({
    shouldUpdate: (props, nextProps) =>
      props.startNode.kind !== nextProps.startNode.kind ||
      props.endNode.kind !== nextProps.endNode.kind ||
      props.startNode.nodeType !== nextProps.startNode.nodeType ||
      props.endNode.nodeType !== nextProps.endNode.nodeType ||
      props.connection !== nextProps.connection,
    calculateProps: ({
      startNode,
      endNode,
      connection,
      nodeTypes,
    }): Partial<PrivateConnectionProps> => ({
      startNodePosition: portPosition(
        nodeTypes,
        startNode,
        connection.start.port,
      ),
      endNodePosition: portPosition(nodeTypes, endNode, connection.end.port),
    }),
  }),
)

export const ConnectionCView = ({
  connection,
  startNode,
  endNode,
  isActive,
  nodeTypes,
  startNodePosition,
  endNodePosition,
}: PrivateConnectionProps) => {
  const startPosition = Point.add(startNode.position, startNodePosition)
  const endPosition = Point.add(endNode.position, endNodePosition)
  const port = getType(nodeTypes, startNode).ports[connection.start.port]

  return (
    <ConnectionLine
      startPosition={startPosition}
      endPosition={endPosition}
      type={isValuePort(port) ? port.dataType : 'Event'}
      isActive={isActive}
    />
  )
}

export const ConnectionC = enhance(ConnectionCView)

export type ConnectionLineProps = {
  startPosition: Point
  endPosition: Point
  type: DataType | 'Event' | 'creating'
  isActive?: boolean
}

export const ConnectionLine = ({
  startPosition,
  endPosition,
  type,
  isActive,
}: ConnectionLineProps) => {
  const start = `${startPosition.x},${startPosition.y}`
  const c1 = `${startPosition.x + 40},${startPosition.y}`
  const c2 = `${endPosition.x - 40},${endPosition.y}`
  const end = `${endPosition.x},${endPosition.y}`
  const path = `M${start} C${c1} ${c2} ${end}`

  return (
    <g>
      <circle cx={startPosition.x} cy={startPosition.y} r="3" fill="white" />
      <circle cx={endPosition.x} cy={endPosition.y} r="3" fill="white" />
      <path
        d={path}
        fill="none"
        stroke={connectionColors[type]}
        strokeWidth={type === 'creating' || type === 'Event' ? 3 : 2}
      />
      {isActive && <Active path={path} />}
    </g>
  )
}
