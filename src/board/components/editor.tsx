import React, {ReactElement} from 'react'
import {CSSProperties, ReactChild} from 'react'
import {FormHelper} from 'react-form-helper'
import {connect} from 'react-redux'
import {ComponentEnhancer, compose} from 'recompose'
import styled from 'styled-components'
import {materialColors} from 'styled-material/lib/colors'
import {
  Awaitable,
  DataType,
  Node,
  NodeType,
  Property,
} from '../../schematic-core/entities'
import {getNode, getType} from '../../schematic-core/node_helpers'
import {EditorState} from '../lib/state/editor'
import {setProperties} from '../lib/state/program'
import {ProgramC} from './program'

export {ComponentEnhancer as _ComponentEnhancer}

class PromiseResolver<T> extends React.Component<
  {
    getValue: (arg: any) => Awaitable<T>
    arg?: any
    children: (value: T) => ReactElement<any> | Array<ReactElement<any>>
  },
  {hasValue: boolean; value: T | undefined}
> {
  state = {hasValue: false, value: undefined}

  componentDidMount() {
    this.getValue()
  }

  componentDidUpdate(prevProps: this['props']) {
    if (this.props.arg !== prevProps.arg) {
      this.getValue()
    }
  }

  private getValue() {
    const result = this.props.getValue(this.props.arg)
    if (result && (result as Promise<T>).then) {
      this.setState({hasValue: false, value: undefined})
      ;(result as Promise<T>).then(value =>
        this.setState({hasValue: true, value}),
      )
    } else {
      this.setState({hasValue: true, value: result as T})
    }
  }

  render() {
    return this.state.hasValue ? this.props.children(this.state.value!) : null
  }
}

const ProgramContainer = styled.div`
  width: 100%;
  height: 100%;
`
const PanelContainer = styled.div`
  width: 320px;
  min-height: 320px;

  color: white;
  background: #333;
  border-top: 1px solid #666;
  border-left: 1px solid #666;

  font-family: Ubuntu, Arial;
  font-size: 14;
`

const PanelTitle = styled.div`
  margin-bottom: 4px;
  padding: 4px 12px;

  background: ${materialColors['blue-500']};
`

const PanelItem = styled.label`
  display: flex;
  box-sizing: border-box;
  padding: 4px 12px;
  width: 100%;

  > input {
    padding: 2px;

    color: white;
    caret-color: #aaa;
    background: #444;
    border: 1px solid #666;

    &[type='number'] {
      text-align: right;
    }
  }
`

type PanelInputProps = {
  label: string
  type: DataType
  value: any
  onChange: (value: any) => void
  rawProperty?: Property
  property: Property
  nodeProperties: {[id: string]: any}
}

const PanelInput = ({
  label,
  type,
  value,
  onChange,
  rawProperty,
  property,
  nodeProperties,
}: PanelInputProps) =>
  type === 'Enum' ? (
    <PanelItem>
      <span style={{flex: 1}}>{label}</span>
      <select value={value || ''} onChange={e => onChange(e.target.value)}>
        <option value={''} />
        <PromiseResolver
          getValue={
            (rawProperty && rawProperty.getValues) ||
            (() => property.values || [])
          }
          arg={nodeProperties}
        >
          {(values: Array<{name: string; value: any}>) =>
            values.map((value, i) => (
              <option key={i} value={value.value}>
                {value.name}
              </option>
            ))
          }
        </PromiseResolver>
      </select>
    </PanelItem>
  ) : (
    <PanelItem>
      <span style={{flex: 1}}>{label}</span>
      <input
        type={type === 'Num' ? 'number' : type === 'Bool' ? 'checkbox' : 'text'}
        value={value}
        onChange={e => onChange(e.target.value)}
      />
    </PanelItem>
  )

const Panel = ({
  title,
  children,
  style,
}: {
  title: string
  children?: ReactChild
  style?: CSSProperties
}) => (
  <PanelContainer style={style}>
    <PanelTitle>{title}</PanelTitle>
    {children}
  </PanelContainer>
)

const PropertiesPanel = ({
  node,
  rawNodeType,
  nodeType,
  setProperties,
}: {
  node: Node
  rawNodeType: NodeType
  nodeType: NodeType
  setProperties: (properties: object) => void
}) =>
  Object.values(nodeType.properties).length > 0 ? (
    <Panel
      title={`${nodeType.fullName || nodeType.name} Properties`}
      style={{position: 'absolute', right: 0, bottom: 0}}
    >
      <FormHelper
        value={node.properties || {}}
        onSave={setProperties}
        onChange={setProperties}
        inputComponent={PanelInput}
        fields={Object.values(nodeType.properties).map(prop => ({
          path: [prop.id],
          label: prop.name,
          type: prop.dataType,
          rawProperty: rawNodeType.properties[prop.id],
          property: prop,
          nodeProperties: node.properties || {},
        }))}
      />
    </Panel>
  ) : null

export type EditorProps = {
  nodeTypes: {[kind: string]: NodeType}
}

export type PrivateEditorProps = EditorProps & {
  selectedNode?: {node: Node; type: NodeType}
  setProperties: (id: string, properties: object) => void
}

export const enhance = compose<PrivateEditorProps, EditorProps>(
  connect(
    (state: EditorState): Partial<PrivateEditorProps> => {
      const node =
        state.selection && state.selection.size === 1
          ? getNode(state.program, state.selection.first()!)
          : undefined

      return {
        nodeTypes: state.program.nodeTypes,
        selectedNode: node && {
          node,
          type: getType(state.program.nodeTypes, node),
        },
      }
    },
    (dispatch): Partial<PrivateEditorProps> => ({
      setProperties: (id, properties) =>
        dispatch(setProperties(id, properties)),
    }),
  ),
)

export const EditorView = ({
  nodeTypes,
  selectedNode,
  setProperties,
}: PrivateEditorProps) => (
  <ProgramContainer>
    <ProgramC nodeTypes={nodeTypes} />
    {selectedNode && (
      <PropertiesPanel
        node={selectedNode.node}
        rawNodeType={nodeTypes[selectedNode.type.id]}
        nodeType={getType(nodeTypes, selectedNode.node)}
        setProperties={p => setProperties(selectedNode.node.id, p)}
      />
    )}
  </ProgramContainer>
)

export const Editor = enhance(EditorView)
