import {partition} from 'ramda'
import React from 'react'
import {connect} from 'react-redux'
import {ComponentEnhancer, compose, shouldUpdate, withState} from 'recompose'
import {materialColors} from 'styled-material/lib/colors'
import {Node, NodeType, NodeTypes} from '../../schematic-core/entities'
import {
  getNode,
  getType,
  isInput,
  isValid,
  isValuePort,
} from '../../schematic-core/node_helpers'
import {Text} from '../../svg/components/text'
import {Point, offset, offsetEnd, translate} from '../../svg/lib/svg_helpers'
import {ContextMenu} from '../../ui/components/context_menu'
import {Draggable} from '../../ui/components/draggable'
import {deleteNode, moveNode, selectNode} from '../lib/actions/nodes'
import {
  eventSeparatorHeight,
  nodeHeadHeight,
  nodeHeight,
  nodeWidth,
  portHeight,
  portPositionOpt,
} from '../lib/measurement'
import {EditorState} from '../lib/state/editor'
import {PortC} from './port'

export {ComponentEnhancer as _ComponentEnhancer}

const errorIcon =
  'M9 1.03c-4.42 0-8 3.58-8 8s3.58 8 8 8 8-3.58 8-8-3.58-8-8-8zM10 13H8v-2h2v2zm0-3H8V5h2v5z'

export type Props = {
  nodeTypes: NodeTypes
  id: string
}

export type PrivateProps = Props & {
  node: Node
  isSelected: boolean
  isValid: boolean
  selectNode: (e: MouseEvent | React.MouseEvent<{}>) => void
  deleteNode: (id: string) => void
  setPosition: (position: Point) => void
  initialPosition: Point
  setInitialPosition: (position: Point) => void
}

const enhance = compose<PrivateProps, Props>(
  connect(
    (state: EditorState, {id}: Props): Partial<PrivateProps> => ({
      node: getNode(state.program, id),
      isSelected: state.selection && state.selection.has(id),
      isValid: isValid(getNode(state.program, id), state.program),
    }),
    (dispatch: any, {id}: Props): Partial<PrivateProps> => ({
      selectNode: e => dispatch(selectNode(e, id)),
      setPosition: position => dispatch(moveNode(id, position)),
      deleteNode: id => dispatch(deleteNode(id)),
    }),
  ),
  withState(
    'initialPosition',
    'setInitialPosition',
    ({node}: PrivateProps) => node.position,
  ),
)

export const NodeCView = ({
  nodeTypes,
  node,
  isSelected,
  isValid,
  selectNode,
  deleteNode,
  setPosition,
  initialPosition,
  setInitialPosition,
}: PrivateProps) => (
  <Draggable
    onClick={selectNode}
    onMove={setPosition}
    onDone={setInitialPosition}
    startPosition={initialPosition}
  >
    {({onMouseDown}) => (
      <ContextMenu
        items={[
          {
            label: 'Delete',
            onClick: () => deleteNode(node.id),
          },
        ]}
        onOpen={selectNode}
      >
        <g
          onMouseDown={onMouseDown}
          transform={translate(node.position.x, node.position.y)}
        >
          <NodeBody
            nodeTypes={nodeTypes}
            isSelected={isSelected}
            isValid={isValid}
            node={node}
          />
        </g>
      </ContextMenu>
    )}
  </Draggable>
)

export const NodeC = enhance(NodeCView)

export type NodeBodyProps = {
  nodeTypes: NodeTypes
  isSelected: boolean
  isValid: boolean
  node: Node
}

export const NodeBodyView = ({
  nodeTypes,
  isSelected,
  isValid,
  node,
}: NodeBodyProps) => {
  const nodeType = getType(nodeTypes, node)
  const width = nodeWidth(nodeTypes, node)
  const height = nodeHeight(nodeTypes, node)

  return (
    <g>
      <rect
        x={-width / 2}
        y={-height / 2}
        width={width}
        height={height}
        fill="rgba(58, 58, 58, 0.7)"
        stroke={isSelected ? 'white' : '#666666'}
        strokeWidth="1"
        rx="2"
        ry="2"
      />

      <Text
        y={offset(height, 15)}
        horizontal="center"
        vertical="center"
        bold
        title={nodeType.fullName}
      >
        {nodeType.name}
      </Text>
      {!isValid && (
        <g>
          <path
            d={errorIcon}
            fill={materialColors['red-500']}
            transform={`${translate(
              offset(width, 3),
              offset(height, 3),
            )} scale(0.8)`}
          />
          <title>The node has required properties that have not been set</title>
        </g>
      )}

      <NodePorts
        nodeTypes={nodeTypes}
        nodeId={node.id}
        nodeHeight={height}
        nodeWidth={width}
        nodeType={nodeType}
      />
    </g>
  )
}

export const NodeBody = shouldUpdate<NodeBodyProps>(
  (oldProps, newProps) =>
    oldProps.isSelected !== newProps.isSelected ||
    oldProps.isValid !== newProps.isValid ||
    oldProps.nodeTypes !== newProps.nodeTypes ||
    oldProps.node.nodeType !== newProps.node.nodeType,
)(NodeBodyView)

export type NodePortsProps = {
  nodeTypes: NodeTypes
  nodeId: string
  nodeWidth: number
  nodeHeight: number
  nodeType: NodeType
}

export const NodePorts = ({
  nodeTypes,
  nodeId,
  nodeWidth,
  nodeHeight,
  nodeType,
}: NodePortsProps) => {
  const nodeSize = {width: nodeWidth, height: nodeHeight}

  const [valuePorts, eventPorts] = partition(
    isValuePort,
    Object.values(nodeType.ports),
  )
  const [eventInputs, eventOutputs] = partition(isInput, eventPorts)
  const [valueInputs, valueOutputs] = partition(isInput, valuePorts)
  const eventCount = Math.max(eventInputs.length, eventOutputs.length)

  return (
    <g>
      <g>
        <g>
          {eventInputs.map((port, i) => (
            <PortC
              key={port.id}
              nodeTypes={nodeTypes}
              node={nodeId}
              port={port}
              position={portPositionOpt(nodeSize, port, i, false)}
            />
          ))}
        </g>
        <g>
          {eventOutputs.map((port, i) => (
            <PortC
              key={port.id}
              nodeTypes={nodeTypes}
              node={nodeId}
              port={port}
              position={portPositionOpt(nodeSize, port, i, false)}
              align="right"
            />
          ))}
        </g>
      </g>

      {valuePorts.length &&
        eventPorts.length && (
          <line
            x1={offset(nodeWidth, 0)}
            x2={offsetEnd(nodeWidth, 0)}
            y1={offset(
              nodeHeight,
              nodeHeadHeight +
                portHeight * (eventCount - 0.5) +
                eventSeparatorHeight / 2,
            )}
            y2={offset(
              nodeHeight,
              nodeHeadHeight +
                portHeight * (eventCount - 0.5) +
                eventSeparatorHeight / 2,
            )}
            stroke={'rgba(255, 255, 255, 0.5)'}
            strokeWidth={0.5}
          />
        )}

      <g>
        <g>
          {valueInputs.map((port, i) => (
            <PortC
              key={port.id}
              nodeTypes={nodeTypes}
              node={nodeId}
              port={port}
              position={portPositionOpt(
                nodeSize,
                port,
                i + eventCount,
                !!eventCount,
              )}
            />
          ))}
        </g>
        <g>
          {valueOutputs.map((port, i) => (
            <PortC
              key={port.id}
              nodeTypes={nodeTypes}
              node={nodeId}
              port={port}
              position={portPositionOpt(
                nodeSize,
                port,
                i + eventCount,
                !!eventCount,
              )}
              align="right"
            />
          ))}
        </g>
      </g>
    </g>
  )
}
