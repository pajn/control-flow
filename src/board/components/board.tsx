import {filterMap} from 'iterates/lib/sync'
import React, {ReactNode} from 'react'
import {connect} from 'react-redux'
import {ComponentEnhancer, compose} from 'recompose'
import styled from 'styled-components'
import {NodeType, NodeTypes} from '../../schematic-core/entities'
import {ContextMenu, ContextMenuItems} from '../../ui/components/context_menu'
import {PanZoom} from '../../ui/components/pan_zoom'
import {createNode} from '../lib/actions/nodes'
import {NodeSeed} from '../lib/state/program'

export {ComponentEnhancer as _ComponentEnhancer}

const StyledBoard = styled.div`
  position: relative;
  flex: 1;
  align-self: stretch;

  font-family: 'Inconsolata', monospace;
  cursor: default;
  user-select: none;
`

export type BoardProps = {
  nodeTypes: {[kind: string]: NodeType}
  onMouseDown?: (e: React.MouseEvent<any>) => void
  onMouseUp?: () => void
  onClick?: () => void
  children: ReactNode
}
export type PrivateBoardProps = BoardProps & {
  createNode: (node: NodeSeed) => void
}

export const enhance = compose<PrivateBoardProps, BoardProps>(
  connect(
    undefined,
    (dispatch): Partial<PrivateBoardProps> => ({
      createNode: (node: NodeSeed) => dispatch(createNode(node)),
    }),
  ),
)

const getContextMenu = (
  nodeTypes: NodeTypes,
  createNode: (e: React.MouseEvent<{}>, kind: string) => void,
): ContextMenuItems => {
  const groups = new Map<string, ContextMenuItems>()
  const set = (key: string, item: ContextMenuItems[0]) => {
    const items = [item]
    groups.set(key, items)
    return items
  }

  return [
    ...filterMap(type => {
      const onMouseDown = (e: React.MouseEvent<any>) => {
        createNode(e, type.id)
      }

      const group = type.group &&
        type.group.length > 0 && {
          label: type.group[0],
          key: type.group[0],
        }
      return group
        ? groups.has(group.key)
          ? (groups.get(group.key)!.push({
              label: type.fullName || type.name,
              onMouseDown,
            }),
            undefined)
          : {
              label: group.label,
              subItems: set(group.key, {
                label: type.fullName || type.name,
                onMouseDown,
              }),
            }
        : {
            label: type.fullName || type.name,
            onMouseDown,
          }
    }, Object.values(nodeTypes)),
  ]
}

export const BoardView = ({
  nodeTypes,
  onMouseDown,
  onMouseUp,
  onClick,
  createNode,
  children,
}: PrivateBoardProps) => (
  <StyledBoard>
    <PanZoom onClick={onClick}>
      {({containerProps, translate}) => (
        <ContextMenu
          items={getContextMenu(nodeTypes, (e, kind) =>
            createNode({
              kind,
              position: {x: e.pageX - translate.x, y: e.pageY - translate.y},
            }),
          )}
        >
          <svg width="100%" height="100%" style={{position: 'absolute'}}>
            <defs>
              <pattern
                id="single"
                x="0"
                y="0"
                width="30"
                height="30"
                patternUnits="userSpaceOnUse"
              >
                <rect x="0" y="0" width="30" height="30" fill="#262626" />

                <rect x="0" y="0" width="2" height="30" fill="#232323" />
                <rect x="0" y="0" width="30" height="2" fill="#232323" />
              </pattern>
              <pattern
                id="background"
                x="0"
                y="0"
                width="120"
                height="120"
                patternUnits="userSpaceOnUse"
              >
                <rect
                  x="0"
                  y="0"
                  width="120"
                  height="120"
                  fill="url(#single)"
                />

                <rect x="0" y="0" width="2" height="120" fill="#202020" />
                <rect x="0" y="0" width="120" height="2" fill="#202020" />
              </pattern>
            </defs>
            <g {...containerProps}>
              <g onMouseDown={onMouseDown} onMouseUp={onMouseUp}>
                <rect
                  x="-200"
                  y="-200"
                  width="2400"
                  height="2400"
                  fill="url(#background)"
                  transform={`translate(${-translate.x}, ${-translate.y}) translate(${translate.x %
                    120}, ${translate.y % 120})`}
                />
                {children}
              </g>
            </g>
          </svg>
        </ContextMenu>
      )}
    </PanZoom>
  </StyledBoard>
)

export const Board = enhance(BoardView)
