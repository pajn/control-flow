import {Graph, Socket} from '../../schematic-core/entities'
import {Point} from '../../svg/lib/svg_helpers'

export type ValueDisplay = {
  position: Point
  socket: Socket
}

export type SerializedEditor = {
  graph: Graph
  valueDisplays: {[id: string]: ValueDisplay}
}
