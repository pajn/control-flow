import {complement, partition, reduce, zipWith} from 'ramda'
import {Node, NodeTypes, Port} from '../../schematic-core/entities'
import {getType, isInput, isValuePort} from '../../schematic-core/node_helpers'
import {Point, offset, offsetEnd} from '../../svg/lib/svg_helpers'

enum TextSize {
  NodeName = 10,
  PortName = 7.2,
}

const textLength = (length: number, size: TextSize) => length * size
const nameLength = (e: {name?: string}) => (e.name && e.name.length) || 0
const max = (a: number, b: number) => Math.max(a, b)
const maxIn = (arr: Array<number>) => reduce(max, 0 as number, arr)
function zipSum<T>(pred: (e: T) => number, arr: [Array<T>, Array<T>]) {
  let a = arr[0]
  let b = arr[1]
  if (a.length < b.length) {
    a.length = b.length
  } else if (a.length > b.length) {
    b.length = a.length
  }
  return zipWith((ae, be) => (ae ? pred(ae) : 0) + (be ? pred(be) : 0), a, b)
}

export const nodeHeadHeight = 30
export const portHeight = 18
export const eventSeparatorHeight = 8

export function nodeWidth(nodeTypes: NodeTypes, node: Node) {
  const nodeType = getType(nodeTypes, node)
  const nameWidth = textLength(nodeType.name.length, TextSize.NodeName)
  const [valuePorts, eventPorts] = partition(
    isValuePort,
    Object.values(nodeType.ports),
  )
  const longestPortLine = maxIn(
    zipSum(nameLength, partition(isInput, eventPorts)).concat(
      zipSum(nameLength, partition(isInput, valuePorts)),
    ),
  )
  const portWidth = 30 + textLength(longestPortLine, TextSize.PortName)
  return 20 + Math.max(nameWidth, portWidth)
}

export function nodeHeight(nodeTypes: NodeTypes, node: Node) {
  const nodeType = getType(nodeTypes, node)
  const [valuePorts, eventPorts] = partition(
    isValuePort,
    Object.values(nodeType.ports),
  )
  const eventPortCount = partition(isInput, eventPorts)
    .map(p => p.length)
    .reduce(max, 0)
  const valuePortCount = partition(isInput, valuePorts)
    .map(p => p.length)
    .reduce(max, 0)

  return (
    nodeHeadHeight +
    (eventPortCount + valuePortCount) * portHeight -
    6 +
    (valuePorts.length && eventPorts.length ? eventSeparatorHeight : 0)
  )
}

export function nodeSize(nodeTypes: NodeTypes, node: Node): Size {
  return {
    width: nodeWidth(nodeTypes, node),
    height: nodeHeight(nodeTypes, node),
  }
}

export type Size = {
  width: number
  height: number
}

export function portPosition(nodeTypes: NodeTypes, node: Node, portId: string) {
  const nodeType = getType(nodeTypes, node)
  const size = nodeSize(nodeTypes, node)
  const port = nodeType.ports[portId]
  const portIsInput = isInput(port)
  const [inputs, outputs] = partition(isInput, Object.values(nodeType.ports))
  const [values, events] = partition(
    isValuePort,
    portIsInput ? inputs : outputs,
  )
  let index = (isValuePort(port) ? values : events).findIndex(p => p === port)

  let addEventSeparatorHeight = false
  if (isValuePort(port)) {
    const eventCount = max(
      events.length,
      (portIsInput ? outputs : inputs).filter(complement(isValuePort)).length,
    )
    addEventSeparatorHeight = !!eventCount
    index += eventCount
  }

  return portPositionOpt(size, port, index, addEventSeparatorHeight)
}

export function portPositionOpt(
  nodeSize: Size,
  port: Port,
  portIndex: number,
  addEventSeparatorHeight: boolean,
): Point {
  return {
    x: isInput(port)
      ? offset(nodeSize.width, 10)
      : offsetEnd(nodeSize.width, 10),
    y:
      offset(nodeSize.height, nodeHeadHeight + portIndex * portHeight) +
      (addEventSeparatorHeight ? eventSeparatorHeight : 0),
  }
}
