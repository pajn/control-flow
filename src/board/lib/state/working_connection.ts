import {Action, createActions, createReducer} from 'redux-decorated'
import {Connection, Socket} from '../../../schematic-core/entities'
import {Point} from '../../../svg/lib/svg_helpers'
import {programActions} from './program'

export type WorkingConnectionAction = 'moving' | 'creating'

export type WorkingConnection = {
  action: WorkingConnectionAction
  previousConnection?: Connection
  socket: Socket
  fromInput: boolean
  startPosition: Point
  relativePosition: Point
  currentPosition: Point
}
export type WorkingConnectionState = null | WorkingConnection

export const workingConnectionActions = createActions({
  setWorkingConnection: {} as Action<WorkingConnection>,
  moveWorkingConnection: {} as Action<{position: Point}>,
  clearWorkingConnection: {} as Action<{}>,
})

export const workingConnectionReducer = createReducer<WorkingConnectionState>(
  null,
)
  .when(
    workingConnectionActions.setWorkingConnection,
    (_, connection) => connection,
  )
  .when(
    workingConnectionActions.moveWorkingConnection,
    (state, {position}) =>
      state && {
        ...state,
        currentPosition: Point.add(state.relativePosition, position),
      },
  )
  .when(workingConnectionActions.clearWorkingConnection, _ => null)
  .when(programActions._createConnection, _ => null)
  .build()
