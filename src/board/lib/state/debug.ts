import {
  Action,
  action,
  createActions,
  createReducer,
  updateIn,
} from 'redux-decorated'
import {Debugger} from '../../../runtime/entities'
import {ValueDisplay} from '../entities'

export type ConnectionId = string
export type Active = boolean

export type DebugState = {
  events: Map<ConnectionId, Active>
  values: Map<ConnectionId, any>
  valueDisplays: Map<string, ValueDisplay>
}

const debugActions = createActions({
  event: {} as Action<{connectionId: ConnectionId; active: Active}>,
  value: {} as Action<{connectionId: ConnectionId; value: any}>,

  loadDisplays: {} as Action<DebugState['valueDisplays']>,
  createDisplay: {} as Action<{id: string; display: ValueDisplay}>,
  deleteDisplay: {} as Action<{id: string}>,
})

export const createDebugger = (dispatch: any): Debugger => ({
  onEvent: connectionId => {
    dispatch(action(debugActions.event, {connectionId, active: true}))
    setTimeout(() => {
      dispatch(action(debugActions.event, {connectionId, active: false}))
    }, 500)
  },
  onValue: (connectionId, value) => {
    dispatch(action(debugActions.value, {connectionId, value}))
  },
})

export const createInitialDebugState = (): DebugState => ({
  events: new Map(),
  values: new Map(),

  valueDisplays: new Map(),
})

export const debugReducer = createReducer<DebugState>(createInitialDebugState())
  .when(debugActions.event, (state, {connectionId, active}) => {
    state.events.set(connectionId, active)
    return state
  })
  .when(debugActions.value, (state, {connectionId, value}) => {
    state.values.set(connectionId, value)
    return state
  })
  .when(debugActions.loadDisplays, (state, displays) =>
    updateIn('valueDisplays', displays, state),
  )
  .when(debugActions.createDisplay, (state, {id, display}) => {
    state.values.set(id, display)
    return state
  })
  .when(debugActions.deleteDisplay, (state, {id}) => {
    state.values.delete(id)
    return state
  })
  .build()
