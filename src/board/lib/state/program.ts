// import {port} from '_debugger'
import {pipeValue} from 'iterates'
import {asArray, collect, map} from 'iterates/cjs/sync'
import {
  Action,
  action,
  createActions,
  createReducer,
  removeIn,
  updateIn,
} from 'redux-decorated'
import {ThunkAction} from 'redux-thunk'
import {
  Connection,
  Node,
  NodeType,
  Program,
} from '../../../schematic-core/entities'
import {
  NodeConnectionIndex,
  createIndex,
  indexConnection,
  removeConnectionFromIndex,
} from '../../../schematic-core/index'
import {getNode, getType} from '../../../schematic-core/node_helpers'
import {Point} from '../../../svg/lib/svg_helpers'
import {EditorState} from './editor'

export type ProgramState = Program & {
  nodeConnectionIndex: NodeConnectionIndex
}

const getConnections = (program: ProgramState, node: Node) => {
  const nodeType = getType(program.nodeTypes, node)
  const nodeConnections = program.nodeConnectionIndex.get(node.id) || {}

  return pipeValue(
    Object.keys(nodeType.ports),
    collect((port) => [
      port,
      pipeValue(
        nodeConnections[port] || new Set(),
        map((connectionId) => program.graph.connections[connectionId]),
        map((connection) =>
          nodeType.ports[port].kind === 'EventInput' ||
          nodeType.ports[port].kind === 'ValueInput'
            ? connection.start
            : connection.end,
        ),
        map(
          (socket) =>
            getType(program.nodeTypes, program.graph.nodes[socket.node]).ports[
              socket.port
            ],
        ),
        asArray,
      ),
    ]),
  )
}

const updateDynamicNodeType = (
  nodeId: string,
): ThunkAction<Promise<void>, EditorState, void> => async (
  dispatch,
  getState,
) => {
  const program = getState().program
  const node = getNode(program, nodeId)
  // Read from nodeTypes instead of using getTypes as we want the "real" version with dynamicOverrides
  const nodeType = program.nodeTypes[node.kind]
  const dynamicNodeType = getType(program.nodeTypes, node)
  if (nodeType.dynamicOverrides !== undefined) {
    const {
      ports = dynamicNodeType.ports,
      properties = dynamicNodeType.properties,
    } = await nodeType.dynamicOverrides(
      node.properties,
      getConnections(program, node),
    )
    dispatch(
      action(programActions._setNodeType, {
        id: nodeId,
        nodeType: {...nodeType, ports, properties, dynamicOverrides: undefined},
      }),
    )
  } else if (node.nodeType) {
    dispatch(
      action(programActions._setNodeType, {
        id: nodeId,
        nodeType: undefined,
      }),
    )
  }
}

export type NodeSeed = {
  kind: string
  position: Point
  properties?: Node['properties']
}

export const programActions = createActions({
  _loadProgram: {} as Action<Program>,

  createNode: {} as Action<Node>,
  moveNode: {} as Action<{id: string; position: Point}>,
  deleteNode: {} as Action<{id: string}>,
  _setProperties: {} as Action<{id: string; properties: object}>,
  _setNodeType: {} as Action<{id: string; nodeType?: NodeType}>,

  _createConnection: {} as Action<Connection>,
  _deleteConnection: {} as Action<{id: string}>,
})

export const loadProgram = (
  program: Program,
): ThunkAction<Promise<void>, EditorState, void> => async (dispatch) => {
  dispatch(action(programActions._loadProgram, program))
  for (const nodeId of Object.keys(program.graph.nodes)) {
    dispatch(updateDynamicNodeType(nodeId))
  }
}

export const setProperties = (
  nodeId: string,
  newProperties: object,
): ThunkAction<Promise<void>, EditorState, void> => async (dispatch) => {
  dispatch(
    action(programActions._setProperties, {
      id: nodeId,
      properties: newProperties,
    }),
  )
  dispatch(updateDynamicNodeType(nodeId))
}

export const createConnection = (
  connection: Connection,
): ThunkAction<Promise<void>, EditorState, void> => async (dispatch) => {
  dispatch(action(programActions._createConnection, connection))
  await dispatch(updateDynamicNodeType(connection.start.node))
  await dispatch(updateDynamicNodeType(connection.end.node))
}

export const deleteConnection = (
  connectionId: string,
): ThunkAction<Promise<void>, EditorState, void> => async (
  dispatch,
  getState,
) => {
  const connection = getState().program.graph.connections[connectionId]
  dispatch(action(programActions._deleteConnection, {id: connectionId}))
  await dispatch(updateDynamicNodeType(connection.start.node))
  await dispatch(updateDynamicNodeType(connection.end.node))
}

export const initialState: ProgramState = {
  graph: {nodes: {}, connections: {}},
  nodeTypes: {},
  nodeConnectionIndex: new Map(),
}

export const programReducer = createReducer<ProgramState>(initialState)
  .when(programActions._loadProgram, (_, {graph, nodeTypes}) => ({
    graph,
    nodeTypes,
    nodeConnectionIndex: createIndex(graph),
  }))
  /**
   * Node management
   */
  .when(programActions.createNode, (state, node) =>
    updateIn(['graph', 'nodes', node.id], node, state),
  )
  .when(programActions.moveNode, (state, {id, position}) =>
    updateIn(['graph', 'nodes', id, 'position'], position, state),
  )
  .when(programActions.deleteNode, (state, {id}) => {
    let node
    if ((node = state.nodeConnectionIndex.get(id))) {
      Object.values(node).forEach((connections) => {
        connections.forEach((connectionId) => {
          const connection = state.graph.connections[connectionId]
          if (!connection) return state
          removeConnectionFromIndex(state.nodeConnectionIndex, connection)
          state = removeIn(['graph', 'connections', connectionId], state)
        })
      })
      state.nodeConnectionIndex.delete(id)
    }
    return removeIn(['graph', 'nodes', id], state)
  })
  .when(programActions._setProperties, (state, {id, properties}) =>
    updateIn(['graph', 'nodes', id, 'properties'], properties, state),
  )
  .when(programActions._setNodeType, (state, {id, nodeType}) =>
    updateIn(['graph', 'nodes', id, 'nodeType'], nodeType, state),
  )
  /**
   * Connection management
   */
  .when(programActions._createConnection, (state, connection) => {
    state = updateIn(['graph', 'connections', connection.id], connection, state)
    indexConnection(state.nodeConnectionIndex, connection)
    return state
  })
  .when(programActions._deleteConnection, (state, {id}) => {
    const connection = state.graph.connections[id]
    if (!connection) return state
    removeConnectionFromIndex(state.nodeConnectionIndex, connection)
    return removeIn(['graph', 'connections', id], state)
  })
  .build()

export function isPortConnected(
  state: ProgramState,
  nodeId: string,
  portId: string,
) {
  let node = state.nodeConnectionIndex.get(nodeId)
  return !!(node && node[portId] && node[portId].size > 0)
}
export function getConnection(
  state: ProgramState,
  nodeId: string,
  portId: string,
) {
  return state.nodeConnectionIndex.get(nodeId)![portId].values().next().value
}
