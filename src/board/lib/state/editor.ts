import {Set} from 'immutable'
import {
  Action,
  createActions,
  createReducer,
  removeIn,
  updateIn,
} from 'redux-decorated'
import {Runtime} from '../../../runtime/entities'
import {DebugState, debugReducer} from './debug'
import {
  ProgramState,
  initialState as initialProgram,
  programActions,
  programReducer,
} from './program'
import {
  WorkingConnectionState,
  workingConnectionReducer,
} from './working_connection'

export type EditorState = {
  program: ProgramState
  workingConnection: WorkingConnectionState
  selection?: Set<string>
  runtime?: Runtime
  debug: DebugState
}

export const editorActions = createActions({
  setRuntime: {} as Action<{runtime?: Runtime}>,

  selectNodes: {} as Action<{selection: Set<string>}>,
  clearSelection: {} as Action<{}>,
})

const initialState: EditorState = {
  program: initialProgram,
  debug: undefined as any,
  workingConnection: null,
}

export const editorReducer = createReducer<EditorState>(initialState)
  .with('debug', debugReducer)
  .with('program', programReducer)
  .with('workingConnection', workingConnectionReducer)
  .when(editorActions.setRuntime, (state, {runtime}) =>
    updateIn('runtime', runtime, state),
  )
  /**
   * Selection management
   */
  .when(editorActions.selectNodes, (state, {selection}) =>
    updateIn('selection', selection, state),
  )
  .when(editorActions.clearSelection, state => removeIn('selection', state))
  /*
   * Runtime management
   */
  .when(programActions.createNode, state => {
    if (state.runtime) {
      state.runtime.updateProgramWithIndex(
        state.program,
        state.program.nodeConnectionIndex,
      )
    }
    return state
  })
  .when(programActions.deleteNode, state => {
    if (state.runtime) {
      state.runtime.updateProgramWithIndex(
        state.program,
        state.program.nodeConnectionIndex,
      )
    }
    return state
  })
  .when(programActions._createConnection, state => {
    if (state.runtime) {
      state.runtime.updateProgramWithIndex(
        state.program,
        state.program.nodeConnectionIndex,
      )
    }
    return state
  })
  .when(programActions._deleteConnection, state => {
    if (state.runtime) {
      state.runtime.updateProgramWithIndex(
        state.program,
        state.program.nodeConnectionIndex,
      )
    }
    return state
  })
  .when(programActions._setProperties, state => {
    if (state.runtime) {
      state.runtime.updateProgramWithIndex(
        state.program,
        state.program.nodeConnectionIndex,
      )
    }
    return state
  })
  .build()
