import {Set} from 'immutable'
import {action} from 'redux-decorated'
import {ThunkAction} from 'redux-thunk'
import uuid from 'uuid/v1'
import {Node} from '../../../schematic-core/entities'
import {getNode, getType} from '../../../schematic-core/node_helpers'
import {Point} from '../../../svg/lib/svg_helpers'
import {EditorState, editorActions} from '../state/editor'
import {NodeSeed, programActions} from '../state/program'

export function selectNode(
  e: MouseEvent | React.MouseEvent<{}>,
  nodeId: string,
): ThunkAction<any, EditorState, any> {
  e.stopPropagation()
  return (dispatch, getState) => {
    const state = getState()
    if ((e.ctrlKey || e.shiftKey) && state.selection) {
      if (state.selection.has(nodeId)) {
        dispatch(
          action(editorActions.selectNodes, {
            selection: state.selection.delete(nodeId),
          }),
        )
      } else {
        dispatch(
          action(editorActions.selectNodes, {
            selection: state.selection.add(nodeId),
          }),
        )
      }
    } else
      dispatch(action(editorActions.selectNodes, {selection: Set([nodeId])}))
  }
}

export function createNode(seed: NodeSeed): ThunkAction<any, EditorState, any> {
  return (dispatch, getState) => {
    const state = getState()
    const id = uuid()
    const properties = {...seed.properties!}
    Object.values(
      getType(state.program.nodeTypes, seed as Node).properties,
    ).forEach((prop) => {
      if (!(prop.id in properties) && 'defaultValue' in prop) {
        properties[prop.id] = prop.defaultValue
      }
    })
    const node: Node = {...seed, id, properties}

    dispatch(action(programActions.createNode, node))
    dispatch(action(editorActions.selectNodes, {selection: Set([id])}))
  }
}

export function moveNode(
  id: string,
  position: Point,
): ThunkAction<any, EditorState, any> {
  return (dispatch, getState) => {
    const state = getState()

    if (state.selection && state.selection.has(id)) {
      const diff = Point.subtract(position, getNode(state.program, id).position)
      state.selection.forEach((id) => {
        const position = Point.add(getNode(state.program, id!).position, diff)
        dispatch(action(programActions.moveNode, {id: id!, position}))
      })
    } else {
      dispatch(action(programActions.moveNode, {id, position}))
    }
  }
}

export function deleteNode(id: string): ThunkAction<any, EditorState, any> {
  return (dispatch, getState) => {
    const state = getState()

    if (state.selection) {
      state.selection.forEach((id) => {
        dispatch(action(programActions.deleteNode, {id}))
      })
    } else {
      dispatch(action(programActions.deleteNode, {id}))
    }
  }
}

export function deleteSelectedNodes(): ThunkAction<any, EditorState, any> {
  return (dispatch, getState) => {
    const state = getState()

    if (state.selection) {
      state.selection.forEach((id) => {
        dispatch(action(programActions.deleteNode, {id}))
      })
    }
  }
}
