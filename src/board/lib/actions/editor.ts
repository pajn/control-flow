import {ThunkAction} from 'redux-thunk'
import {NodeTypes} from '../../../schematic-core/entities'
import {SerializedEditor} from '../entities'
import {EditorState} from '../state/editor'
import {loadProgram} from '../state/program'

export function save(): ThunkAction<any, EditorState, any> {
  return (_, getState) => {
    const state = getState()
    const toStore: SerializedEditor = {
      graph: state.program.graph,
      valueDisplays: {},
    }

    localStorage.setItem('editor', JSON.stringify(toStore))
  }
}

export function load(nodeTypes: NodeTypes): ThunkAction<any, EditorState, any> {
  return (dispatch, getState) => {
    const json = localStorage.getItem('editor')

    if (json) {
      const stored: SerializedEditor = JSON.parse(json)

      dispatch(loadProgram({graph: stored.graph, nodeTypes}))
    } else {
      dispatch(
        loadProgram({
          graph: getState().program.graph,
          nodeTypes,
        }),
      )
    }
  }
}
