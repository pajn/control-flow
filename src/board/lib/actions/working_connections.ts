import {action} from 'redux-decorated'
import {ThunkAction} from 'redux-thunk'
import uuid from 'uuid/v1'
import {NodeTypes, Socket, ValuePort} from '../../../schematic-core/entities'
import {
  getNode,
  getPort,
  isInput,
  isValuePort,
} from '../../../schematic-core/node_helpers'
import {Point} from '../../../svg/lib/svg_helpers'
import {portPosition} from '../measurement'
import {EditorState} from '../state/editor'
import {
  createConnection,
  deleteConnection,
  getConnection,
  isPortConnected,
} from '../state/program'
import {workingConnectionActions} from '../state/working_connection'

function abortWorkingConnection(dispatch: any, state: EditorState) {
  if (!state.workingConnection) return
  if (state.workingConnection.previousConnection) {
    const connection = state.workingConnection.previousConnection
    dispatch(createConnection(connection))
  }
  dispatch(action(workingConnectionActions.clearWorkingConnection, undefined))
}

export function startCreateConnection(
  nodeTypes: NodeTypes,
  socket: Socket,
): ThunkAction<any, EditorState, any> {
  return (dispatch, getState) => {
    const state = getState()
    const node = getNode(state.program, socket.node)
    const position = Point.add(
      node.position,
      portPosition(nodeTypes, node, socket.port),
    )

    dispatch(
      action(workingConnectionActions.setWorkingConnection, {
        action: 'creating',
        socket,
        fromInput: isInput(getPort(state.program, socket)),
        startPosition: position,
        relativePosition: position,
        currentPosition: position,
      }),
    )
  }
}

export function startMovingConnection(
  nodeTypes: NodeTypes,
  socket: Socket,
): ThunkAction<any, EditorState, any> {
  return (dispatch, getState) => {
    const state = getState()
    const connectionId = getConnection(state.program, socket.node, socket.port)
    const connection = state.program.graph.connections[connectionId]
    const otherSocket =
      connection.end.node === socket.node && connection.end.port === socket.port
        ? connection.start
        : connection.end
    const node = getNode(state.program, socket.node)
    const otherNode = getNode(state.program, otherSocket.node)
    const position = Point.add(
      node.position,
      portPosition(nodeTypes, node, socket.port),
    )

    dispatch(
      action(workingConnectionActions.setWorkingConnection, {
        action: 'moving' as 'moving',
        previousConnection: connection,
        socket: otherSocket,
        fromInput: isInput(getPort(state.program, otherSocket)),
        startPosition: Point.add(
          otherNode.position,
          portPosition(nodeTypes, otherNode, otherSocket.port),
        ),
        relativePosition: position,
        currentPosition: position,
      }),
    )
    dispatch(deleteConnection(connectionId))
  }
}

export function completeWorkingConnection(
  socket: Socket,
  force: boolean,
): ThunkAction<any, EditorState, any> {
  return (dispatch, getState) => {
    const state = getState()

    if (isInput(getPort(state.program, state.workingConnection!.socket))) {
      const tmp = state.workingConnection!.socket
      state.workingConnection!.socket = socket
      socket = tmp
    }

    const start = getPort(state.program, state.workingConnection!.socket)
    const end = getPort(state.program, socket)
    const startNode = state.workingConnection!.socket.node

    if (
      isInput(start) === isInput(end) ||
      ((start as ValuePort).dataType !== (end as ValuePort).dataType &&
        !(
          (start as ValuePort).dataType === 'Any' ||
          (end as ValuePort).dataType === 'Any'
        ))
    ) {
      console.debug('Aborting working connection due to I/O or type mismatch')
      return abortWorkingConnection(dispatch, state)
    }

    if (
      isValuePort(start) &&
      isPortConnected(state.program, socket.node, end.id)
    ) {
      if (force) {
        const oldConnection = getConnection(state.program, socket.node, end.id)
        dispatch(deleteConnection(oldConnection))
      } else {
        console.debug(
          'Aborting working connection due to value port connected to full input port',
        )
        return abortWorkingConnection(dispatch, state)
      }
    }

    if (
      !isValuePort(start) &&
      isPortConnected(state.program, startNode, start.id)
    ) {
      if (force) {
        const oldConnection = getConnection(state.program, startNode, start.id)
        dispatch(deleteConnection(oldConnection))
      } else {
        console.debug(
          'Aborting working connection due to event port connected to full output port',
        )
        return abortWorkingConnection(dispatch, state)
      }
    }

    const id = uuid()
    const connection = {
      id,
      start: state.workingConnection!.socket,
      end: socket,
    }

    dispatch(createConnection(connection))
  }
}
