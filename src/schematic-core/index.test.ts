import 'babel-polyfill'
import {createIndex, indexConnection, removeConnectionFromIndex} from './index'
import {connection, fakeGraph, fakeNode, fakeNodes} from './test_helpers'

const testGraph = fakeGraph(
  [fakeNode(), fakeNode(), fakeNode(), fakeNode()],
  [
    [[0, 'a'], [2, 'a']],
    [[1, 'a'], [2, 'a']],
    [[2, 'b'], [3, 'a']],
    [[2, 'b'], [1, 'b']],
  ],
)

describe('schematic-core', () => {
  describe('index', () => {
    describe('createIndex', () => {
      it('should include all connections in the index', () => {
        const index = createIndex(testGraph)
        expect(index.size).toBe(4)
        expect(index.get('0')!['a']).toEqual(new Set(['0']))
        expect(index.get('2')!['a']).toEqual(new Set(['0', '1']))
        expect(index.get('1')!['a']).toEqual(new Set(['1']))
        expect(index.get('2')!['b']).toEqual(new Set(['2', '3']))
        expect(index.get('3')!['a']).toEqual(new Set(['2']))
        expect(index.get('1')!['b']).toEqual(new Set(['3']))
      })
    })

    describe('indexConnection', () => {
      it('should add a new connection between two nodes', () => {
        const index = createIndex(fakeGraph(fakeNodes(2)))
        indexConnection(index, connection(0, [0, 'a'], [1, 'a']))
        expect(index.size).toBe(2)
        expect(index.get('0')!['a']).toEqual(new Set(['0']))
        expect(index.get('1')!['a']).toEqual(new Set(['0']))
      })
      it('should add a new connection between the same node', () => {
        const emptyIndex = createIndex(fakeGraph(fakeNodes(1)))
        indexConnection(emptyIndex, connection(0, [0, 'a'], [0, 'a']))
        expect(emptyIndex.size).toBe(1)
        expect(emptyIndex.get('0')!['a']).toEqual(new Set(['0']))
        expect(emptyIndex.get('0')!['a']).toEqual(new Set(['0']))
      })
      it('should add a connection between the two node', () => {
        const index = createIndex(testGraph)
        indexConnection(index, connection('target', [3, 'a'], [2, 'a']))
        expect(index.size).toBe(4)
        expect(index.get('2')!['a']).toEqual(new Set(['0', '1', 'target']))
        expect(index.get('3')!['a']).toEqual(new Set(['2', 'target']))
      })
    })

    describe('removeConnectionFromIndex', () => {
      it('should remove the passed connection', () => {
        const index = createIndex(testGraph)
        removeConnectionFromIndex(index, connection('0', [0, 'a'], [2, 'a']))
        expect(index.get('0')!['a']).toEqual(new Set([]))
        expect(index.get('2')!['a']).toEqual(new Set(['1']))
      })
    })
  })
})
