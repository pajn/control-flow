import {Connection, Graph, Node} from './entities'

export const fakeNode = (seed: Partial<Node> = {}): Node => ({
  id: '',
  kind: '',
  position: {x: 0, y: 0},
  properties: {},
  ...seed,
})

export const fakeNodes = (count: number, seed?: Partial<Node>) =>
  Array.prototype.map.call({length: count}, () => fakeNode(seed)) as Array<Node>

export const connection = (
  id: any,
  start: [any, string],
  end: [any, string],
): Connection => ({
  id: `${id}`,
  start: {node: `${start[0]}`, port: start[1]},
  end: {node: `${end[0]}`, port: end[1]},
})

export const fakeGraph = (
  nodes: Array<Node> = [],
  connections: Array<[[any, string], [any, string]]> = [],
) => {
  const graph: Graph = {nodes: {}, connections: {}}
  nodes.forEach((node, i) => {
    if (!node.id) {
      node.id = `${i}`
    }
    graph.nodes[node.id] = node
  })

  connections.forEach(([start, end], i) => {
    const connection: Connection = {
      id: `${i}`,
      start: {node: `${start[0]}`, port: start[1]},
      end: {node: `${end[0]}`, port: end[1]},
    }
    graph.connections[connection.id] = connection
  })

  return graph
}
