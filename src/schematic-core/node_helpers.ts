import {
  Environment,
  Node,
  NodeTypes,
  Port,
  Program,
  Socket,
  ValuePort,
} from './entities'

export const typeColors = {
  Any: 'white',
  Bool: '#e02929',
  Num: '#62afc7',
  String: '#c8e029',
  Enum: '#e0aa29',
  Object: '#e869f0',
}

export const isValid = (node: Node, program: Program) =>
  Object.values(getType(program.nodeTypes, node).properties).every(
    prop => !(prop.required && !(prop.id in node.properties)),
  )

export const isInput = (p: Port) =>
  p.kind === 'ValueInput' || p.kind === 'EventInput'
export const isValuePort = (p: Port): p is Port & ValuePort =>
  p.kind === 'ValueInput' || p.kind === 'ValueOutput'

export function getImplementation(
  node: Node,
  implementations: Environment['implementations'],
) {
  return implementations[node.kind]
}
export const getType = (nodeTypes: NodeTypes, node: Node) =>
  node.nodeType || nodeTypes[node.kind]
export const getNode = (program: Program, id: string) => program.graph.nodes[id]

export const getPort = (program: Program, socket: Socket) =>
  getType(program.nodeTypes, program.graph.nodes[socket.node]).ports[
    socket.port
  ]

export const portColor = (port: Port) =>
  isValuePort(port) ? typeColors[port.dataType] : 'white'
