import {Connection, Graph, Socket} from './entities'

export type NodeConnectionIndex = Map<
  string,
  {[port: string]: Set</* connectionId */ string>}
>

export function indexSocket(
  index: NodeConnectionIndex,
  socket: Socket,
  connectionId: string,
) {
  let node
  if ((node = index.get(socket.node))) {
    let connections
    if ((connections = node[socket.port])) {
      connections.add(connectionId)
    } else {
      node[socket.port] = new Set([connectionId])
    }
  } else {
    index.set(socket.node, {[socket.port]: new Set([connectionId])})
  }
}
export function indexConnection(
  index: NodeConnectionIndex,
  connection: Connection,
) {
  indexSocket(index, connection.start, connection.id)
  indexSocket(index, connection.end, connection.id)
}
export function removeSocketFromIndex(
  index: NodeConnectionIndex,
  socket: Socket,
  connectionId: string,
) {
  let node
  if ((node = index.get(socket.node))) {
    let connections
    if ((connections = node[socket.port])) {
      connections.delete(connectionId)
    }
  }
}
export function removeConnectionFromIndex(
  index: NodeConnectionIndex,
  connection: Connection,
) {
  removeSocketFromIndex(index, connection.start, connection.id)
  removeSocketFromIndex(index, connection.end, connection.id)
}

export function createIndex(graph: Graph) {
  const nodeConnectionIndex = new Map() as NodeConnectionIndex
  Object.values(graph.connections).forEach(connection => {
    indexConnection(nodeConnectionIndex, connection)
  })
  return nodeConnectionIndex
}
