import {Point} from '../svg/lib/svg_helpers'

export type PortId = string
export type Awaitable<T> = Promise<T> | T

export type DataType = 'Bool' | 'Num' | 'String' | 'Object' | 'Enum' | 'Any'

export type BasePort = {id: string; name?: string}
export type ValuePort = BasePort & {
  dataType: DataType
  values?: Array<{name: string; value: string}>
  properties?: Record<string, ValuePort>
  getValues?: (
    properties: {[id: string]: any},
  ) => Awaitable<Array<{name: string; value: string}>>
}
export type EventInput = BasePort & {}
export type EventOutput = BasePort & {}
export type ValueInput = ValuePort & {}
export type ValueOutput = ValuePort & {}
export type Property = ValuePort & {defaultValue?: any; required?: boolean}

export type Port =
  | ({kind: 'EventOutput'} & EventOutput)
  | ({kind: 'EventInput'} & EventInput)
  | ({kind: 'ValueInput'} & ValueInput)
  | ({kind: 'ValueOutput'} & ValueOutput)

export type Group = {
  id: string
  name: string
}

export type NodeController = {
  getPorts: () => Array<Port>
  getPort: (portId: PortId) => any
  isConnected: (portId: PortId) => boolean
  getProperty: (propertyId: string) => any

  dispatchEvent: (portId: PortId) => void

  state?: any
}

export type NodeTypeImplementation = {
  id: string

  onStart?: (ctrl: NodeController) => void
  onStop?: (ctrl: NodeController) => void
  onPropertiesChanged?: (ctrl: NodeController, oldProperties: any) => void

  onEvent?: (portId: PortId, ctrl: NodeController) => void
  getPort?: (portId: PortId, ctrl: NodeController) => any
}

export type NodeType = {
  id: string
  name: string
  fullName?: string
  group?: Array<string>
  ports: Record<string, Port>
  properties: Record<string, Property>

  dynamicOverrides?: (
    properties: Record<string, any>,
    connections: Map<PortId, Array<Port>>,
  ) => Awaitable<{
    ports?: Record<string, Port>
    properties?: Record<string, Property>
  }>
}
export type NodeTypes = {[kind: string]: NodeType}

export type Node = {
  id: string
  kind: string
  position: Point
  properties: {[id: string]: any}

  nodeType?: NodeType
}

export type Connection = {id: string; start: Socket; end: Socket}
export type Socket = {
  node: string
  port: string
}

export type Graph = {
  nodes: {[id: string]: Node}
  connections: {[id: string]: Connection}
}

export type Program = {
  graph: Graph
  nodeTypes: {[id: string]: NodeType}
}

export type Environment = {
  program: Program
  implementations: {[id: string]: NodeTypeImplementation}
}
