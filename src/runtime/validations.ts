import {NodeType, NodeTypeImplementation} from '../schematic-core/entities'

export function validateEnvironment({
  program: {nodeTypes},
  implementations,
}: {
  program: {nodeTypes: {[id: string]: NodeType}}
  implementations: {[id: string]: NodeTypeImplementation}
}) {
  const types = Object.keys(nodeTypes)
  types.forEach(type => {
    if (nodeTypes[type].id !== type)
      throw Error(`Type ${type} has an invalid id`)
    if (!implementations[type])
      throw Error(`Type ${type} has no implementation`)
    if (implementations[type].id !== type)
      throw Error(`Implementation ${type} has an invalid id`)

    validateImplementation(nodeTypes[type], implementations[type])
  })
}

export function validateImplementation(
  nodeType: NodeType,
  implementation: NodeTypeImplementation,
): implementation is NodeTypeImplementation & {
  getPort: NodeTypeImplementation['getPort']
} {
  if (
    nodeType.ports &&
    Object.values(nodeType.ports).filter(port => port.kind === 'ValueOutput')
      .length > 0 &&
    !implementation.getPort
  ) {
    throw Error(
      `Implementation for ${nodeType.id} is missing a getPort implementation`,
    )
  }
  if (
    nodeType.ports &&
    Object.values(nodeType.ports).filter(port => port.kind === 'EventInput')
      .length > 0 &&
    !implementation.onEvent
  ) {
    throw Error(
      `Implementation for ${nodeType.id} is missing an onEvent implementation`,
    )
  }
  return true
}
