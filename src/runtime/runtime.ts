import {
  Environment,
  Node,
  NodeController,
  Program,
} from '../schematic-core/entities'
import {NodeConnectionIndex, createIndex} from '../schematic-core/index'
import {getImplementation, getNode} from '../schematic-core/node_helpers'
import {createController} from './controller'
import {Debugger, GetConnectedNode, GetProgram, Runtime} from './entities'
import {validateEnvironment} from './validations'

export function createRuntime(
  environment: Environment,
  debugger_?: Debugger,
): Runtime {
  const nodeConnectionIndex = createIndex(environment.program.graph)
  return createRuntimeWithIndex(environment, nodeConnectionIndex, debugger_)
}

export function createRuntimeWithIndex(
  environment: Environment,
  nodeConnectionIndex: NodeConnectionIndex,
  debugger_?: Debugger,
): Runtime {
  let {program} = environment

  validateEnvironment(environment)
  const nodeControllerIndex = new Map<string, NodeController>()

  const getProgram: GetProgram = () => program
  const getConnectedNode: GetConnectedNode = (node, port, direction) => {
    const nodeConnections = nodeConnectionIndex.get(node)
    const connections = nodeConnections && nodeConnections[port]
    const connectionId = connections && connections.values().next().value
    if (!connectionId || !program.graph.connections[connectionId])
      return undefined
    const otherSocket = program.graph.connections[connectionId][direction]
    const otherCtrl = nodeControllerIndex.get(otherSocket.node)!
    const otherImpl = getImplementation(
      getNode(program, otherSocket.node),
      environment.implementations,
    )

    return {connectionId, otherSocket, otherCtrl, otherImpl}
  }

  function initializeNodes(nodes: Array<Node>) {
    nodes.forEach(node => {
      const ctrl = createController(
        node,
        getProgram,
        getConnectedNode,
        debugger_,
      )
      nodeControllerIndex.set(node.id, ctrl)
    })
  }

  function startNodes(nodes: Array<Node>) {
    nodes.forEach(node => {
      const impl = getImplementation(node, environment.implementations)

      if (!impl) {
        throw new Error(`No Impl for node type ${node.kind}`)
      }

      if (impl.onStart) {
        const ctrl = nodeControllerIndex.get(node.id)!
        impl.onStart(ctrl)
      }
    })
  }

  function stopNodes(nodes: Array<Node>) {
    nodes.forEach(node => {
      const impl = getImplementation(node, environment.implementations)

      if (impl.onStop) {
        const ctrl = nodeControllerIndex.get(node.id)!
        impl.onStop(ctrl)
      }
    })
  }

  initializeNodes(Object.values(program.graph.nodes))

  return {
    start() {
      startNodes(Object.values(program.graph.nodes))
    },
    stop() {
      stopNodes(Object.values(program.graph.nodes))
    },
    updateProgram(updatedProgram: Program) {
      const nodeConnectionIndex = createIndex(environment.program.graph)
      this.updateProgramWithIndex(updatedProgram, nodeConnectionIndex)
    },
    updateProgramWithIndex(
      updatedProgram: Program,
      updatedIndex: NodeConnectionIndex,
    ) {
      const updatedEnvironment = {
        program: updatedProgram,
        implementations: environment.implementations,
      }
      validateEnvironment(updatedEnvironment)

      const addedNodes: Array<Node> = []
      const removedNodes: Array<Node> = []
      const propertiesChanged: Array<[Node, Node]> = []

      const allIds = new Set(
        Object.keys(program.graph.nodes).concat(
          Object.keys(updatedProgram.graph.nodes),
        ),
      )

      allIds.forEach(nodeId => {
        const oldNode = program.graph.nodes[nodeId]
        const newNode = updatedProgram.graph.nodes[nodeId]
        if (!oldNode) {
          addedNodes.push(newNode)
        } else if (!newNode) {
          removedNodes.push(oldNode)
        } else if (oldNode.properties !== newNode.properties) {
          propertiesChanged.push([oldNode, newNode])
        }
      })

      initializeNodes(addedNodes)

      environment = updatedEnvironment
      program = updatedProgram
      nodeConnectionIndex = updatedIndex

      stopNodes(removedNodes)
      startNodes(addedNodes)

      propertiesChanged.forEach(([oldNode, newNode]) => {
        const impl = getImplementation(newNode, environment.implementations)
        if (impl.onPropertiesChanged) {
          const ctrl = nodeControllerIndex.get(newNode.id)!
          impl.onPropertiesChanged(ctrl, oldNode.properties)
        }
      })
    },
  }
}
