import {
  NodeController,
  NodeTypeImplementation,
  Program,
  Socket,
} from '../schematic-core/entities'
import {NodeConnectionIndex} from '../schematic-core/index'

export type Runtime = {
  start: () => void
  stop: () => void

  updateProgram: (updatedProgram: Program) => void
  updateProgramWithIndex: (
    updatedProgram: Program,
    updatedIndex: NodeConnectionIndex,
  ) => void
}

export type Debugger = {
  onEvent: (connectionId: string) => void
  onValue: (connectionId: string, value: any) => void
}

export type GetProgram = () => Program
export type GetConnectedNode = (
  node: string,
  port: string,
  direction: 'start' | 'end',
) => {
  connectionId: string
  otherSocket: Socket
  otherCtrl: NodeController
  otherImpl: NodeTypeImplementation
} | undefined
