import {Node, NodeController} from '../schematic-core/entities'
import {getNode, getType} from '../schematic-core/node_helpers'
import {Debugger, GetConnectedNode, GetProgram} from './entities'

export function createController(
  node: Node,
  getProgram: GetProgram,
  getConnectedNode: GetConnectedNode,
  debugger_?: Debugger,
) {
  const nodeId = node.id
  const type = getType(getProgram().nodeTypes, node)

  const ctrl: NodeController = {
    getPorts: () => Object.values(type.ports),
    getPort: portId => {
      if (!type.ports[portId])
        throw Error(`Missing port ${portId} on type ${type.id}`)
      if (type.ports[portId].kind !== 'ValueInput')
        throw Error(`Can only read values on value input ports`)

      const connectedNode = getConnectedNode(nodeId, portId, 'start')
      if (!connectedNode) return
      const {connectionId, otherSocket, otherCtrl, otherImpl} = connectedNode

      const value = otherImpl.getPort!(otherSocket.port, otherCtrl)

      if (debugger_) {
        debugger_.onValue(connectionId, value)
      }

      return value
    },
    isConnected: portId => {
      if (!type.ports[portId])
        throw Error(`Missing port ${portId} on type ${type.id}`)

      const portKind = type.ports[portId].kind
      const direction =
        portKind === 'ValueInput' || portKind === 'EventInput' ? 'start' : 'end'
      const connectedNode = getConnectedNode(nodeId, portId, direction)

      return !!connectedNode
    },
    getProperty: propertyId => {
      if (!type.properties[propertyId])
        throw Error(`Missing property ${propertyId} on type ${type.id}`)
      return getNode(getProgram(), nodeId).properties[propertyId]
    },
    dispatchEvent: portId => {
      if (!type.ports[portId])
        throw Error(`Missing port ${portId} on type ${type.id}`)
      if (type.ports[portId].kind !== 'EventOutput')
        throw Error(`Can only dispatch events on event output ports`)

      const connectedNode = getConnectedNode(nodeId, portId, 'end')
      if (!connectedNode) return
      const {connectionId, otherSocket, otherCtrl, otherImpl} = connectedNode

      if (debugger_) {
        debugger_.onEvent(connectionId)
      }

      otherImpl.onEvent!(otherSocket.port, otherCtrl)
    },
  }

  return ctrl
}
