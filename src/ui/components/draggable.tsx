import React, {ReactNode} from 'react'
import {Point} from '../../svg/lib/svg_helpers'

export type DraggableProps = {
  startPosition?: Point
  filterStartEvent?: (e: React.MouseEvent<SVGElement>) => boolean
  onClick?: (e: MouseEvent) => void
  onStart?: (position: Point) => void
  onMove?: (position: Point) => void
  onDone?: (position: Point) => void
  children: (
    props: {onMouseDown: (e: React.MouseEvent<SVGElement>) => void},
  ) => ReactNode
}

export type State = {
  isMonitoring: boolean
  isDragging: boolean
  initialOffset: Point
  initialRealOffset: Point
  offset?: Point
  panOffset: Point
}

declare interface SVGElement {
  getScreenCTM: () => DOMMatrix | null
}
export {SVGElement}

export class Draggable extends React.Component<DraggableProps, State> {
  state: State = {
    isMonitoring: false,
    isDragging: false,
    initialRealOffset: {x: 0, y: 0},
    initialOffset: {x: 0, y: 0},
    panOffset: {x: 0, y: 0},
  }

  onMouseDown = (e: React.MouseEvent<SVGElement>) => {
    if (e.buttons !== 1) return
    if (
      this.props.filterStartEvent !== undefined &&
      !this.props.filterStartEvent(e)
    )
      return
    e.stopPropagation()
    const ctm = e.currentTarget.getScreenCTM()

    this.state.isMonitoring = true
    this.state.panOffset = {
      x: ctm ? ctm.e : 0,
      y: ctm ? ctm.f : 0,
    }
    const position = {
      x: e.pageX - this.state.panOffset.x,
      y: e.pageY - this.state.panOffset.y,
    }
    this.state.initialRealOffset = position
    this.state.initialOffset = position
    if (this.props.startPosition) {
      this.state.initialOffset.x -= this.props.startPosition.x
      this.state.initialOffset.y -= this.props.startPosition.y
    }
    this.state.offset = undefined
    document.addEventListener('mousemove', this.onMouseMove)
    document.addEventListener('mouseup', this.onMouseUp)
  }
  onMouseMove = (e: MouseEvent) => {
    e.stopPropagation()
    const position = {
      x: e.pageX - this.state.panOffset.x,
      y: e.pageY - this.state.panOffset.y,
    }

    this.state.offset = {
      x: position.x - this.state.initialOffset.x,
      y: position.y - this.state.initialOffset.y,
    }
    if (this.state.isDragging) {
      if (this.props.onMove) {
        this.props.onMove(this.state.offset)
      }
    } else {
      if (Point.distance(this.state.initialRealOffset, position) > 5) {
        this.state.isDragging = true
        if (this.props.onStart) {
          this.props.onStart(this.state.initialOffset)
        }
      }
    }
  }
  onMouseUp = (e: MouseEvent) => {
    e.stopPropagation()

    const isDragging = this.state.isDragging
    this.state.isMonitoring = false
    this.state.isDragging = false

    document.removeEventListener('mousemove', this.onMouseMove)
    document.removeEventListener('mouseup', this.onMouseUp)

    if (isDragging) {
      if (this.props.onDone && this.state.offset) {
        this.props.onDone(this.state.offset)
      }
    } else {
      if (this.props.onClick) {
        this.props.onClick(e)
      }
    }
  }

  componentWillUnmount() {
    if (this.state.isMonitoring) {
      document.removeEventListener('mousemove', this.onMouseMove)
      document.removeEventListener('mouseup', this.onMouseUp)
    }
  }

  render() {
    const {children} = this.props
    return children({
      onMouseDown: this.onMouseDown,
    })
  }
}
