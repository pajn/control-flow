import React, {ReactNode} from 'react'
import {Point} from '../../svg/lib/svg_helpers'
import {Draggable} from './draggable'

export type PanZoomContainerProps = {
  transform: string
  onMouseDown: React.MouseEventHandler<SVGElement>
}

export type PanZoomParameters = {
  translate: {x: number; y: number}
  containerProps: PanZoomContainerProps
}
declare interface SVGElement {
  getScreenCTM: () => DOMMatrix | null
}
export {SVGElement}

export class PanZoom extends React.Component<
  {
    onClick?: () => void
    children: (params: PanZoomParameters) => ReactNode
  },
  {
    isDragging: boolean
    liveTranslate: Point
    translate: Point
  }
> {
  state = {
    isDragging: false,
    translate: {x: 0, y: 0},
    liveTranslate: {x: 0, y: 0},
  }

  onMove = (position: Point) => {
    this.setState({
      liveTranslate: position,
    })
  }
  onDone = (position: Point) => {
    this.setState({
      translate: position,
    })
  }

  render() {
    const {liveTranslate: translate} = this.state

    return (
      <Draggable
        onClick={this.props.onClick}
        startPosition={this.state.translate}
        filterStartEvent={e => !e.shiftKey}
        onMove={this.onMove}
        onDone={this.onDone}
      >
        {({onMouseDown}) =>
          this.props.children({
            translate,
            containerProps: {
              transform: `translate(${translate.x}, ${translate.y})`,
              onMouseDown,
            },
          })
        }
      </Draggable>
    )
  }
}
