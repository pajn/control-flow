import Popper from 'popper.js'
import PropTypes from 'prop-types'
import React, {
  CSSProperties,
  Children,
  Component,
  ComponentType,
  ReactElement,
  Ref,
  cloneElement,
  createRef,
} from 'react'
import {getContext} from 'recompose'
import styled from 'styled-components'

export type ContextMenuItems = Array<ContextMenuItem>
export type ContextMenuItem = {
  label: string
  onClick?: (e: React.MouseEvent<HTMLElement>) => void
  onMouseDown?: (e: React.MouseEvent<HTMLElement>) => void
  subItems?: ContextMenuItems
}
export type ContextMenuState = {
  position: {top: number; left: number}
  items: ContextMenuItems
}

const contextMenuContext = {
  showMenu: PropTypes.func,
}

const ContextMenuReference = styled.div`
  position: absolute;
  width: 0;
  height: 0;
`
const ContextMenuContainer = styled.div`
  margin-top: ${(props: {noOffset?: boolean}) => (props.noOffset ? -5 : 0)}px;
  padding-top: 4px;
  padding-bottom: 4px;

  min-width: 200px;

  color: white;
  background: #333;
  border: 1px solid #999;

  font-family: Ubuntu, Arial;
  font-size: 14;
  cursor: default;
`
const ContextMenuItemContainer = styled.div`
  display: flex;
  padding: 4px 8px;

  &:hover {
    background: #ff5233;
  }
`

function wrapIf<
  ChildrenProps,
  ParentProps extends {children: ReactElement<ChildrenProps>}
>(
  shouldWrap: boolean,
  Parent: React.ComponentType<ParentProps>,
  parentProps: Pick<ParentProps, Exclude<keyof ParentProps, 'children'>>,
  children: ReactElement<ChildrenProps>,
) {
  return shouldWrap ? (
    <Parent {...(parentProps as any)}>{children}</Parent>
  ) : (
    children
  )
}

class SubContextMenu extends Component<
  {
    children: ReactElement<{
      innerRef: Ref<HTMLElement>
      onMouseEnter: React.MouseEventHandler<HTMLElement>
      onMouseLeave: React.MouseEventHandler<HTMLElement>
    }>
    item: ContextMenuItem
  },
  {overMenuItem: boolean; overMenu: boolean}
> {
  state = {overMenuItem: false, overMenu: false}
  parentRef = createRef<HTMLElement>()
  popper: Popper | undefined

  setRef = (element: HTMLElement | null) => {
    if (element) {
      if (!this.popper) {
        this.popper = new Popper(this.parentRef.current!, element, {
          placement: 'right-start',
          positionFixed: true,
        })
      }
    } else if (this.popper) {
      this.popper.destroy()
      this.popper = undefined
    }
  }

  render() {
    const {children, item} = this.props
    const {overMenu, overMenuItem} = this.state
    const isOpen = overMenu || overMenuItem

    return (
      <>
        {cloneElement(children, {
          innerRef: this.parentRef,
          onMouseEnter: item.subItems
            ? () => this.setState({overMenuItem: true})
            : undefined,
          onMouseLeave: item.subItems
            ? () => this.setState({overMenuItem: false})
            : undefined,
        })}
        {isOpen && item.subItems && (
          <>
            <ContextMenuContainer
              noOffset
              innerRef={this.setRef}
              onMouseEnter={() => this.setState({overMenu: true})}
              onMouseLeave={() => this.setState({overMenu: false})}
            >
              {item.subItems.map((item, i) => (
                <ContextMenuItem key={i} item={item} />
              ))}
            </ContextMenuContainer>
          </>
        )}
      </>
    )
  }
}

const ContextMenuItem = ({item}: {item: ContextMenuItem}) =>
  wrapIf(
    item.subItems !== undefined,
    SubContextMenu,
    {item},
    <ContextMenuItemContainer
      tabIndex={1}
      className="context-menu-item"
      onMouseDown={item.onMouseDown}
      onClick={item.onClick}
    >
      <span style={{flex: 1}}>{item.label}</span>
      {item.subItems && <>&#9654;</>}
    </ContextMenuItemContainer>,
  )

function isMenuItem(e: HTMLElement) {
  return !!e.closest('.context-menu-item')
}

export class ContextMenuProvider extends Component<
  {style?: CSSProperties},
  {menu?: ContextMenuState}
> {
  static childContextTypes = contextMenuContext
  state = {} as {menu?: ContextMenuState}
  menuRef: HTMLElement | null = null
  referenceRef: HTMLElement | null = null
  popper: Popper | undefined

  getChildContext() {
    return {
      showMenu: (menu: ContextMenuState) => {
        this.setState({menu})
      },
    }
  }

  setRef = (ref: 'menu' | 'reference') => (element: HTMLElement | null) => {
    if (ref === 'menu') {
      this.menuRef = element
    } else if (ref === 'reference') {
      this.referenceRef = element
    }

    if (this.menuRef && this.referenceRef) {
      if (!this.popper) {
        this.popper = new Popper(this.referenceRef, this.menuRef, {
          placement: 'right-start',
          positionFixed: true,
        })
      }
    } else if (this.popper) {
      this.popper.destroy()
      this.popper = undefined
    }
  }

  render() {
    const {style} = this.props
    const {menu} = this.state

    return (
      <div
        style={style}
        onContextMenu={(e) => {
          if (menu && !e.ctrlKey) {
            e.preventDefault()
          }
        }}
        onMouseDown={
          !menu
            ? undefined
            : (e) => {
                if (!isMenuItem(e.target as HTMLElement)) {
                  e.stopPropagation()
                  this.setState({menu: undefined})
                }
              }
        }
        onMouseDownCapture={
          !menu
            ? undefined
            : (e) => {
                if (e.buttons === 2) return
                if (!isMenuItem(e.target as HTMLElement)) {
                  e.stopPropagation()
                  this.setState({menu: undefined})
                }
              }
        }
        onClick={
          !menu
            ? undefined
            : (e) => {
                if (isMenuItem(e.target as HTMLElement)) {
                  e.stopPropagation()
                  this.setState({menu: undefined})
                }
              }
        }
      >
        {this.props.children}
        {}
        {menu && (
          <>
            <ContextMenuReference
              style={menu.position}
              innerRef={this.setRef('reference')}
            />
            <ContextMenuContainer innerRef={this.setRef('menu')}>
              {menu.items.map((item, i) => (
                <ContextMenuItem key={i} item={item} />
              ))}
            </ContextMenuContainer>
          </>
        )}
      </div>
    )
  }
}

export type ContextMenuProps = {
  items: ContextMenuItems
  children: ReactElement
  onOpen?: (e: React.MouseEvent<any>) => void
}

export const ContextMenu: ComponentType<ContextMenuProps> = getContext(
  contextMenuContext,
)(
  ({
    items,
    children,
    showMenu,
    onOpen,
    ...props
  }: ContextMenuProps & {showMenu: (menu: ContextMenuState) => void}) =>
    cloneElement(Children.only(children), {
      ...props,
      onMouseDown: (e: React.MouseEvent<any>) => {
        if (!e.ctrlKey && e.buttons === 2) {
          e.preventDefault()
          e.stopPropagation()
          showMenu({items, position: {top: e.pageY, left: e.pageX}})
          if (onOpen) {
            onOpen(e)
          }
        } else if (typeof (props as any).onMouseDown === 'function') {
          return (props as any).onMouseDown(e)
        } else if (
          typeof Children.only(children).props.onMouseDown === 'function'
        ) {
          return Children.only(children).props.onMouseDown(e)
        }
      },
    }),
)
