import React, {ComponentType} from 'react'
import {compose, setDisplayName, withState, wrapDisplayName} from 'recompose'
import {Point, Rect} from '../../svg/lib/svg_helpers'
import {Draggable} from '../components/draggable'

export type Hoc<A, B> = (cmp: ComponentType<A>) => ComponentType<B>

export type Options<P> = {
  clearSelection: (ownProps: P) => void
  onSelected: (area: Rect, ownProps: P) => void
}

export type SelectionAreaProvidedProps = {
  onMouseDown: (e: React.MouseEvent<any>) => void
  selectionArea?: Rect
}

export type PrivateSelectionAreaProps = {
  selectionAreaStart?: Point
  setSelectionAreaStart: (p: Point | undefined) => void
  selectionAreaSize?: Point
  setSelectionAreaSize: (p: Point | undefined) => void
}

export const enhance: <P>(
  cmp: any,
) => ComponentType<P & SelectionAreaProvidedProps> = compose(
  withState('selectionAreaStart', 'setSelectionAreaStart', undefined),
  withState('selectionAreaSize', 'setSelectionAreaSize', undefined),
) as any

export function withSelectionArea<P>({
  clearSelection,
  onSelected,
}: Options<P>): Hoc<P, P & SelectionAreaProvidedProps> {
  return (WrappedComponent: any) =>
    enhance<P>(
      setDisplayName<PrivateSelectionAreaProps>(
        wrapDisplayName(WrappedComponent, 'withSelectionArea'),
      )(
        ({
          selectionAreaStart,
          setSelectionAreaStart,
          selectionAreaSize,
          setSelectionAreaSize,
          ...props
        }: PrivateSelectionAreaProps) => (
          <Draggable
            startPosition={selectionAreaStart}
            onClick={() => clearSelection(props as P)}
            filterStartEvent={e => e.shiftKey}
            onStart={setSelectionAreaStart}
            onMove={setSelectionAreaSize}
            onDone={() => {
              if (selectionAreaStart && selectionAreaSize) {
                onSelected(
                  [
                    selectionAreaStart,
                    Point.add(selectionAreaStart, selectionAreaSize),
                  ],
                  props as P,
                )
              }
              setSelectionAreaSize(undefined)
              setSelectionAreaStart(undefined)
            }}
          >
            {({onMouseDown}) => (
              <WrappedComponent
                {...props}
                onMouseDown={onMouseDown}
                selectionArea={
                  selectionAreaStart &&
                  selectionAreaSize && [selectionAreaStart, selectionAreaSize]
                }
              />
            )}
          </Draggable>
        ),
      ),
    )
}
