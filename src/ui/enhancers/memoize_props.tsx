import React from 'react'
import {wrapDisplayName} from 'recompose'

export type Options<P> = {
  shouldUpdate: (props: P, nextProps: P) => boolean
  calculateProps: (props: P) => any
}

export type State = {
  memoizedProps: object
}

export function withMemoizedProps<P>({
  shouldUpdate,
  calculateProps,
}: Options<P>) {
  return (WrappedComponent: any) =>
    class extends React.Component<P, State> {
      static displayName = wrapDisplayName(
        WrappedComponent,
        'withMemoizedProps',
      )

      componentWillMount() {
        this.setState({
          memoizedProps: calculateProps(this.props),
        })
      }

      componentDidMount() {
        if (!this.state) {
          this.setState({
            memoizedProps: calculateProps(this.props),
          })
        }
      }

      componentWillReceiveProps(nextProps: P) {
        if (shouldUpdate(this.props, nextProps)) {
          this.setState({
            memoizedProps: calculateProps(nextProps),
          })
        }
      }

      render() {
        return (
          <WrappedComponent {...this.props} {...this.state.memoizedProps} />
        )
      }
    }
}
