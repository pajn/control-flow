import {validateEnvironment} from './runtime/validations'
import {
  NodeController,
  NodeType,
  NodeTypeImplementation,
  Port,
} from './schematic-core/entities'

export function mockRuntime(
  nodeTypes: {[id: string]: NodeType},
  implementations: {[id: string]: NodeTypeImplementation},
) {
  validateEnvironment({program: {nodeTypes}, implementations})

  const exec: {
    (
      nodeId: string,
      portValues: {[id: string]: any},
      propertyValues: {[id: string]: any},
    ): {[portId: string]: any}
    dynamicOverride: (
      nodeId: string,
      node: {
        properties?: Record<string, any>
        connections?: Record<string, Array<Port>>
      },
    ) => Promise<void>
  } = (nodeId, portValues, propertyValues) => {
    const nodeType = nodeTypes[nodeId]
    const implementation = implementations[nodeId]

    const ctrl: NodeController = {
      getPorts: () => Object.values(nodeType.ports),
      getPort: id => portValues[id],
      isConnected: id => id in portValues,
      getProperty: id => propertyValues[id],
      dispatchEvent: () => {},
    }

    const outputs: {[portId: string]: any} = {}
    Object.values(nodeType.ports)
      .filter(port => port.kind === 'ValueOutput')
      .forEach(port => {
        outputs[port.id] = implementation.getPort!(port.id, ctrl)
      })

    return outputs
  }

  exec.dynamicOverride = async (nodeId, node) => {
    const {ports} = await nodeTypes[nodeId].dynamicOverrides!(
      node.properties || {},
      new Map(Object.entries(node.connections || {})),
    )
    nodeTypes = {
      ...nodeTypes,
      [nodeId]: {...nodeTypes[nodeId], ports: ports || nodeTypes[nodeId].ports},
    }
  }

  return exec
}
