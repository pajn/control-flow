import {mockRuntime} from '../test-helpers'
import {implementations, nodeTypes} from './logic'

describe('nodes', () => {
  describe('logic', () => {
    const runtime = mockRuntime(nodeTypes, implementations)

    describe('if', () => {
      it('should follow logic rules', () => {})
    })

    describe('and', () => {
      it('should follow logic rules', () => {
        expect(runtime('and', {a: false, b: false}, {})).toEqual({out: false})
        expect(runtime('and', {a: true, b: false}, {})).toEqual({out: false})
        expect(runtime('and', {a: false, b: true}, {})).toEqual({out: false})
        expect(runtime('and', {a: true, b: true}, {})).toEqual({out: true})
      })
    })

    describe('or', () => {
      it('should follow logic rules', () => {
        expect(runtime('or', {a: false, b: false}, {})).toEqual({out: false})
        expect(runtime('or', {a: true, b: false}, {})).toEqual({out: true})
        expect(runtime('or', {a: false, b: true}, {})).toEqual({out: true})
        expect(runtime('or', {a: true, b: true}, {})).toEqual({out: true})
      })
    })

    describe('not', () => {
      it('should follow logic rules', () => {
        expect(runtime('not', {in: false}, {})).toEqual({out: true})
        expect(runtime('not', {in: true}, {})).toEqual({out: false})
      })
    })

    describe('nand', () => {
      it('should follow logic rules', () => {
        expect(runtime('nand', {a: false, b: false}, {})).toEqual({out: true})
        expect(runtime('nand', {a: true, b: false}, {})).toEqual({out: true})
        expect(runtime('nand', {a: false, b: true}, {})).toEqual({out: true})
        expect(runtime('nand', {a: true, b: true}, {})).toEqual({out: false})
      })
    })

    describe('nor', () => {
      it('should follow logic rules', () => {
        expect(runtime('nor', {a: false, b: false}, {})).toEqual({out: true})
        expect(runtime('nor', {a: true, b: false}, {})).toEqual({out: false})
        expect(runtime('nor', {a: false, b: true}, {})).toEqual({out: false})
        expect(runtime('nor', {a: true, b: true}, {})).toEqual({out: false})
      })
    })

    describe('xor', () => {
      it('should follow logic rules', () => {
        expect(runtime('xor', {a: false, b: false}, {})).toEqual({out: false})
        expect(runtime('xor', {a: true, b: false}, {})).toEqual({out: true})
        expect(runtime('xor', {a: false, b: true}, {})).toEqual({out: true})
        expect(runtime('xor', {a: true, b: true}, {})).toEqual({out: false})
      })
    })

    describe('lt', () => {
      it('should follow logic rules', () => {
        expect(runtime('lt', {a: 0, b: 1}, {})).toEqual({out: true})
        expect(runtime('lt', {a: 1, b: 1}, {})).toEqual({out: false})
        expect(runtime('lt', {a: 2, b: 1}, {})).toEqual({out: false})
      })
    })

    describe('lte', () => {
      it('should follow logic rules', () => {
        expect(runtime('lte', {a: 0, b: 1}, {})).toEqual({out: true})
        expect(runtime('lte', {a: 1, b: 1}, {})).toEqual({out: true})
        expect(runtime('lte', {a: 2, b: 1}, {})).toEqual({out: false})
      })
    })

    describe('gt', () => {
      it('should follow logic rules', () => {
        expect(runtime('gt', {a: 0, b: 1}, {})).toEqual({out: false})
        expect(runtime('gt', {a: 1, b: 1}, {})).toEqual({out: false})
        expect(runtime('gt', {a: 2, b: 1}, {})).toEqual({out: true})
      })
    })

    describe('gte', () => {
      it('should follow logic rules', () => {
        expect(runtime('gte', {a: 0, b: 1}, {})).toEqual({out: false})
        expect(runtime('gte', {a: 1, b: 1}, {})).toEqual({out: true})
        expect(runtime('gte', {a: 2, b: 1}, {})).toEqual({out: true})
      })
    })
  })
})
