import {mockRuntime} from '../test-helpers'
import {implementations, nodeTypes} from './utilities'

describe('nodes', () => {
  describe('utilities', () => {
    const runtime = mockRuntime(nodeTypes, implementations)

    describe('deconstructObject', () => {
      it('should deconstruct an object', async () => {
        await runtime.dynamicOverride('deconstructObject', {
          connections: {
            in: [
              {
                id: 'in',
                kind: 'ValueOutput',
                dataType: 'Object',
                properties: {
                  a: {id: 'a', dataType: 'String'},
                  b: {id: 'b', dataType: 'Num'},
                },
              },
            ],
          },
        })

        expect(runtime('deconstructObject', {in: {a: 'a', b: 2}}, {})).toEqual({
          obj_a: 'a',
          obj_b: 2,
        })
      })
    })

    describe('constructObject', () => {
      it('should construct an object', async () => {
        await runtime.dynamicOverride('constructObject', {
          connections: {
            out: [
              {
                id: 'out',
                kind: 'ValueInput',
                dataType: 'Object',
                properties: {
                  a: {id: 'a', dataType: 'String'},
                  b: {id: 'b', dataType: 'Num'},
                },
              },
            ],
          },
        })

        expect(runtime('constructObject', {obj_a: 'a', obj_b: 2}, {})).toEqual({
          out: {
            a: 'a',
            b: 2,
          },
        })
      })
    })
  })
})
