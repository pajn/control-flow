import {tuple} from 'iterates'
import {fromPairs} from 'ramda'
import {
  NodeTypeImplementation,
  NodeTypes,
  ValueInput,
  ValueOutput,
} from '../schematic-core/entities'

export const nodeTypes: NodeTypes = {
  boolValue: {
    id: 'boolValue',
    name: 'Bool',
    group: ['Constants'],
    ports: {
      value: {id: 'value', kind: 'ValueOutput', dataType: 'Bool'},
    },
    properties: {
      value: {id: 'value', name: 'value', dataType: 'Bool', required: true},
    },
  },
  numValue: {
    id: 'numValue',
    name: 'Number',
    group: ['Constants'],
    ports: {
      value: {id: 'value', kind: 'ValueOutput', dataType: 'Num'},
    },
    properties: {
      value: {id: 'value', name: 'value', dataType: 'Num', required: true},
    },
  },
  stringValue: {
    id: 'stringValue',
    name: 'String',
    group: ['Constants'],
    ports: {
      value: {id: 'value', kind: 'ValueOutput', dataType: 'String'},
    },
    properties: {
      value: {id: 'value', name: 'value', dataType: 'String', required: true},
    },
  },
  enumValue: {
    id: 'enumValue',
    name: 'Enum Value',
    group: ['Constants'],
    ports: {
      value: {id: 'value', kind: 'ValueOutput', dataType: 'Enum', values: []},
    },
    properties: {
      value: {
        id: 'value',
        name: 'value',
        dataType: 'Enum',
        required: true,
        values: [],
      },
    },

    dynamicOverrides(_, connections) {
      const {values} = connections.get('value')![0] as ValueInput
      return {
        properties: {
          value: {
            id: 'value',
            name: 'value',
            dataType: 'Enum',
            required: true,
            values,
          },
        },
      }
    },
  },
  sequence: {
    id: 'sequence',
    name: 'Sequence',
    ports: {
      in: {id: 'in', kind: 'EventInput'},
      out_1: {id: 'out_1', kind: 'EventOutput'},
      out_2: {id: 'out_2', kind: 'EventOutput'},
      out_3: {id: 'out_3', kind: 'EventOutput'},
      out_4: {id: 'out_4', kind: 'EventOutput'},
    },
    properties: {},
  },
  deconstructObject: {
    id: 'deconstructObject',
    name: 'from object',
    ports: {
      in: {id: 'in', kind: 'ValueInput', dataType: 'Object', properties: {}},
    },
    properties: {},

    dynamicOverrides(_, connections) {
      const {properties = {}} = (connections.get('in')![0] as ValueOutput) || {}
      return {
        ports: {
          in: {
            id: 'in',
            kind: 'ValueInput',
            dataType: 'Object',
            properties,
          },
          ...fromPairs(
            Object.values(properties).map(prop =>
              tuple([
                `obj_${prop.id}`,
                {
                  ...prop,
                  kind: 'ValueOutput',
                  id: `obj_${prop.id}`,
                  name: prop.id,
                },
              ]),
            ),
          ),
        },
      }
    },
  },
  constructObject: {
    id: 'constructObject',
    name: 'as object',
    ports: {
      out: {id: 'out', kind: 'ValueOutput', dataType: 'Object', properties: {}},
    },
    properties: {},

    dynamicOverrides(_, connections) {
      const {properties = {}} = (connections.get('out')![0] as ValueInput) || {}
      return {
        ports: {
          out: {
            id: 'out',
            kind: 'ValueOutput',
            dataType: 'Object',
            properties,
          },
          ...fromPairs(
            Object.values(properties).map(prop =>
              tuple([
                `obj_${prop.id}`,
                {
                  ...prop,
                  kind: 'ValueInput',
                  id: `obj_${prop.id}`,
                  name: prop.id,
                },
              ]),
            ),
          ),
        },
      }
    },
  },
}

export const implementations: {[kind: string]: NodeTypeImplementation} = {
  sequence: {
    id: 'sequence',
    onEvent: (_, ctrl) => {
      ctrl.dispatchEvent('out_1')
      ctrl.dispatchEvent('out_2')
      ctrl.dispatchEvent('out_3')
      ctrl.dispatchEvent('out_4')
    },
  },
  deconstructObject: {
    id: 'deconstructObject',
    getPort: (name, ctrl) =>
      ctrl.getPort('in') && ctrl.getPort('in')[name.slice(4)],
  },
  constructObject: {
    id: 'constructObject',
    getPort: (_, ctrl) => {
      return fromPairs(
        ctrl
          .getPorts()
          .filter(port => port.id.startsWith('obj_'))
          .map(port => tuple([port.name!, ctrl.getPort(port.id)])),
      )
    },
  },
  boolValue: {
    id: 'boolValue',

    getPort(id, ctrl) {
      return !!ctrl.getProperty(id)
    },
  },
  numValue: {
    id: 'numValue',

    getPort(id, ctrl) {
      return +ctrl.getProperty(id)
    },
  },
  stringValue: {
    id: 'stringValue',

    getPort(id, ctrl) {
      return ctrl.getProperty(id)
    },
  },
  enumValue: {
    id: 'enumValue',

    getPort(id, ctrl) {
      return ctrl.getProperty(id)
    },
  },
}
