import {
  NodeType,
  NodeTypeImplementation,
  Port,
  ValueInput,
  ValuePort,
} from '../schematic-core/entities'

export const nodeTypes: {[kind: string]: NodeType} = {
  if: {
    id: 'if',
    name: 'if',
    fullName: 'If',
    group: ['Logic'],
    ports: {
      in: {id: 'in', kind: 'EventInput'},
      condition: {
        id: 'condition',
        kind: 'ValueInput',
        name: 'condition',
        dataType: 'Bool',
      },
      true: {id: 'true', kind: 'EventOutput', name: 'true'},
      false: {id: 'false', kind: 'EventOutput', name: 'false'},
    },
    properties: {},
  },
  and: {
    id: 'and',
    name: 'and',
    fullName: 'And',
    group: ['Logic'],
    ports: {
      a: {id: 'a', kind: 'ValueInput', name: 'a', dataType: 'Bool'},
      b: {id: 'b', kind: 'ValueInput', name: 'b', dataType: 'Bool'},
      out: {id: 'out', kind: 'ValueOutput', dataType: 'Bool'},
    },
    properties: {},
  },
  or: {
    id: 'or',
    name: 'or',
    fullName: 'Or',
    group: ['Logic'],
    ports: {
      a: {id: 'a', kind: 'ValueInput', name: 'a', dataType: 'Bool'},
      b: {id: 'b', kind: 'ValueInput', name: 'b', dataType: 'Bool'},
      out: {id: 'out', kind: 'ValueOutput', dataType: 'Bool'},
    },
    properties: {},
  },
  not: {
    id: 'not',
    name: 'not',
    fullName: 'Not',
    group: ['Logic'],
    ports: {
      in: {id: 'in', kind: 'ValueInput', name: 'in', dataType: 'Bool'},
      out: {id: 'out', kind: 'ValueOutput', dataType: 'Bool'},
    },
    properties: {},
  },
  nand: {
    id: 'nand',
    name: 'nand',
    fullName: 'Nand',
    group: ['Logic'],
    ports: {
      a: {id: 'a', kind: 'ValueInput', name: 'a', dataType: 'Bool'},
      b: {id: 'b', kind: 'ValueInput', name: 'b', dataType: 'Bool'},
      out: {id: 'out', kind: 'ValueOutput', dataType: 'Bool'},
    },
    properties: {},
  },
  nor: {
    id: 'nor',
    name: 'nor',
    fullName: 'Nor',
    group: ['Logic'],
    ports: {
      a: {id: 'a', kind: 'ValueInput', name: 'a', dataType: 'Bool'},
      b: {id: 'b', kind: 'ValueInput', name: 'b', dataType: 'Bool'},
      out: {id: 'out', kind: 'ValueOutput', dataType: 'Bool'},
    },
    properties: {},
  },
  xor: {
    id: 'xor',
    name: 'xor',
    fullName: 'Xor',
    group: ['Logic'],
    ports: {
      a: {id: 'a', kind: 'ValueInput', name: 'a', dataType: 'Bool'},
      b: {id: 'b', kind: 'ValueInput', name: 'b', dataType: 'Bool'},
      out: {id: 'out', kind: 'ValueOutput', dataType: 'Bool'},
    },
    properties: {},
  },

  lt: {
    id: 'lt',
    name: '<',
    fullName: 'Less Than',
    group: ['Comparisons'],
    ports: {
      a: {id: 'a', kind: 'ValueInput', name: 'a', dataType: 'Num'},
      b: {id: 'b', kind: 'ValueInput', name: 'b', dataType: 'Num'},
      out: {id: 'out', kind: 'ValueOutput', dataType: 'Bool'},
    },
    properties: {},
  },
  lte: {
    id: 'lte',
    name: '<=',
    fullName: 'Less Than Or Equal To',
    group: ['Comparisons'],
    ports: {
      a: {id: 'a', kind: 'ValueInput', name: 'a', dataType: 'Num'},
      b: {id: 'b', kind: 'ValueInput', name: 'b', dataType: 'Num'},
      out: {id: 'out', kind: 'ValueOutput', dataType: 'Bool'},
    },
    properties: {},
  },
  gt: {
    id: 'gt',
    name: '>',
    fullName: 'Greater Than',
    group: ['Comparisons'],
    ports: {
      a: {id: 'a', kind: 'ValueInput', name: 'a', dataType: 'Num'},
      b: {id: 'b', kind: 'ValueInput', name: 'b', dataType: 'Num'},
      out: {id: 'out', kind: 'ValueOutput', dataType: 'Bool'},
    },
    properties: {},
  },
  gte: {
    id: 'gte',
    name: '>=',
    fullName: 'Greater Than Or Equal To',
    group: ['Comparisons'],
    ports: {
      a: {id: 'a', kind: 'ValueInput', name: 'a', dataType: 'Num'},
      b: {id: 'b', kind: 'ValueInput', name: 'b', dataType: 'Num'},
      out: {id: 'out', kind: 'ValueOutput', dataType: 'Bool'},
    },
    properties: {},
  },
  eqn: {
    id: 'eqn',
    name: 'Number equals',
    group: ['Comparisons'],
    ports: {
      a: {id: 'a', kind: 'ValueInput', name: 'a', dataType: 'Num'},
      b: {id: 'b', kind: 'ValueInput', name: 'b', dataType: 'Num'},
      out: {id: 'out', kind: 'ValueOutput', dataType: 'Bool'},
    },
    properties: {},
  },
  eqs: {
    id: 'eqs',
    name: 'String equals',
    group: ['Comparisons'],
    ports: {
      a: {id: 'a', kind: 'ValueInput', name: 'a', dataType: 'String'},
      b: {id: 'b', kind: 'ValueInput', name: 'b', dataType: 'String'},
      out: {id: 'out', kind: 'ValueOutput', dataType: 'Bool'},
    },
    properties: {},
  },
  eqe: {
    id: 'eqe',
    name: 'Enum equals',
    group: ['Comparisons'],
    ports: {
      a: {id: 'a', kind: 'ValueInput', name: 'a', dataType: 'Enum'},
      b: {id: 'b', kind: 'ValueInput', name: 'b', dataType: 'Enum'},
      out: {id: 'out', kind: 'ValueOutput', dataType: 'Bool'},
    },
    properties: {},

    async dynamicOverrides(_, connections) {
      const a = connections.get('a')![0] as ValueInput | undefined
      const b = connections.get('b')![0] as ValueInput | undefined

      const ports: Record<string, Port & ValuePort> = {
        a: {
          id: 'a',
          kind: 'ValueInput',
          name: 'a',
          dataType: 'Enum',
        },
        b: {
          id: 'b',
          kind: 'ValueInput',
          name: 'b',
          dataType: 'Enum',
        },
        out: {id: 'out', kind: 'ValueOutput', dataType: 'Bool'},
      }

      if (a && !b) {
        ports.b.values = a.values
      } else if (b && !a) {
        ports.a.values = b.values
      } else if (!a && !b) {
        ports.a.values = []
        ports.b.values = []
      } else {
        return {}
      }

      return {ports}
    },
  },
  eq: {
    id: 'eq',
    name: 'Equals',
    group: ['Comparisons'],
    ports: {
      a: {id: 'a', kind: 'ValueInput', name: 'a', dataType: 'Any'},
      b: {id: 'b', kind: 'ValueInput', name: 'b', dataType: 'Any'},
      out: {id: 'out', kind: 'ValueOutput', dataType: 'Bool'},
    },
    properties: {},

    async dynamicOverrides(_, connections) {
      const a = connections.get('a')![0] as ValueInput | undefined
      const b = connections.get('b')![0] as ValueInput | undefined

      if (a && b) return {}

      const ports: Record<string, Port & ValuePort> = {
        a: {
          id: 'a',
          kind: 'ValueInput',
          name: 'a',
          dataType: 'Any',
        },
        b: {
          id: 'b',
          kind: 'ValueInput',
          name: 'b',
          dataType: 'Any',
        },
        out: {id: 'out', kind: 'ValueOutput', dataType: 'Bool'},
      }

      if (a && !b) {
        ports.a.dataType = a.dataType
        ports.b.dataType = a.dataType
        if (a.dataType === 'Enum') {
          ports.a.values = a.values
          ports.b.values = a.values
        } else if (a.dataType === 'Object') {
          ports.a.properties = a.properties
          ports.b.properties = a.properties
        }
      } else if (b && !a) {
        ports.a.dataType = b.dataType
        ports.b.dataType = b.dataType
        if (b.dataType === 'Enum') {
          ports.a.values = b.values
          ports.b.values = b.values
        } else if (b.dataType === 'Object') {
          ports.a.properties = b.properties
          ports.b.properties = b.properties
        }
      }

      return {ports}
    },
  },
}

export const implementations: {[kind: string]: NodeTypeImplementation} = {
  if: {
    id: 'if',

    onEvent: (_, ctrl) => {
      if (ctrl.getPort('condition')) {
        ctrl.dispatchEvent('true')
      } else {
        ctrl.dispatchEvent('false')
      }
    },
  },
  and: {
    id: 'and',

    getPort(_, ctrl) {
      return ctrl.getPort('a') && ctrl.getPort('b')
    },
  },
  or: {
    id: 'or',

    getPort(_, ctrl) {
      return ctrl.getPort('a') || ctrl.getPort('b')
    },
  },
  not: {
    id: 'not',

    getPort(_, ctrl) {
      return !ctrl.getPort('in')
    },
  },
  nand: {
    id: 'nand',

    getPort(_, ctrl) {
      return !(ctrl.getPort('a') && ctrl.getPort('b'))
    },
  },
  nor: {
    id: 'nor',

    getPort(_, ctrl) {
      return !(ctrl.getPort('a') || ctrl.getPort('b'))
    },
  },
  xor: {
    id: 'xor',

    getPort(_, ctrl) {
      return ctrl.getPort('a') !== ctrl.getPort('b')
    },
  },

  lt: {
    id: 'lt',

    getPort(_, ctrl) {
      return ctrl.getPort('a') < ctrl.getPort('b')
    },
  },
  lte: {
    id: 'lte',

    getPort(_, ctrl) {
      return ctrl.getPort('a') <= ctrl.getPort('b')
    },
  },
  gt: {
    id: 'gt',

    getPort(_, ctrl) {
      return ctrl.getPort('a') > ctrl.getPort('b')
    },
  },
  gte: {
    id: 'gte',

    getPort(_, ctrl) {
      return ctrl.getPort('a') >= ctrl.getPort('b')
    },
  },
  eqn: {
    id: 'eqn',

    getPort(_, ctrl) {
      return ctrl.getPort('a') === ctrl.getPort('b')
    },
  },
  eqs: {
    id: 'eqs',

    getPort(_, ctrl) {
      return ctrl.getPort('a') === ctrl.getPort('b')
    },
  },
  eqe: {
    id: 'eqe',

    getPort(_, ctrl) {
      return ctrl.getPort('a') === ctrl.getPort('b')
    },
  },
  eq: {
    id: 'eq',

    getPort(_, ctrl) {
      return ctrl.getPort('a') === ctrl.getPort('b')
    },
  },
}
