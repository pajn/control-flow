import {mockRuntime} from '../test-helpers'
import {implementations, nodeTypes} from './math'

describe('nodes', () => {
  describe('math', () => {
    const runtime = mockRuntime(nodeTypes, implementations)

    describe('+', () => {
      it('should add numbers', () => {
        expect(runtime('+', {a: 0, b: -10}, {})).toEqual({out: -10})
        expect(runtime('+', {a: -10, b: 10}, {})).toEqual({out: 0})
        expect(runtime('+', {a: 10, b: 10}, {})).toEqual({out: 20})
      })
    })

    describe('-', () => {
      it('should subtract numbers', () => {
        expect(runtime('-', {a: 0, b: -10}, {})).toEqual({out: 10})
        expect(runtime('-', {a: -10, b: 10}, {})).toEqual({out: -20})
        expect(runtime('-', {a: 10, b: 10}, {})).toEqual({out: 0})
      })
    })

    describe('*', () => {
      it('should multiply numbers', () => {
        expect(runtime('*', {a: 0, b: -10}, {})).toEqual({out: -0})
        expect(runtime('*', {a: -10, b: 10}, {})).toEqual({out: -100})
        expect(runtime('*', {a: 10, b: 10}, {})).toEqual({out: 100})
      })
    })

    describe('/', () => {
      it('should divide numbers', () => {
        expect(runtime('/', {a: 0, b: -10}, {})).toEqual({out: -0})
        expect(runtime('/', {a: -10, b: 10}, {})).toEqual({out: -1})
        expect(runtime('/', {a: 10, b: 10}, {})).toEqual({out: 1})
      })
    })

    describe('pow', () => {
      it('should take the power of numbers', () => {
        expect(runtime('pow', {a: 0, b: -10}, {})).toEqual({out: Infinity})
        expect(runtime('pow', {a: -10, b: 10}, {})).toEqual({out: 1e10})
        expect(runtime('pow', {a: 10, b: 10}, {})).toEqual({out: 1e10})
      })
    })

    describe('max', () => {
      it('should take the max of two numbers', () => {
        expect(runtime('max', {a: 0, b: -10}, {})).toEqual({out: 0})
        expect(runtime('max', {a: -10, b: 10}, {})).toEqual({out: 10})
        expect(runtime('max', {a: 10, b: 10}, {})).toEqual({out: 10})
      })
    })

    describe('min', () => {
      it('should take the min of two numbers', () => {
        expect(runtime('min', {a: 0, b: -10}, {})).toEqual({out: -10})
        expect(runtime('min', {a: -10, b: 10}, {})).toEqual({out: -10})
        expect(runtime('min', {a: 10, b: 10}, {})).toEqual({out: 10})
      })
    })

    describe('abs', () => {
      it('should take the abs of a number', () => {
        expect(runtime('abs', {in: 10}, {})).toEqual({out: 10})
        expect(runtime('abs', {in: -10}, {})).toEqual({out: 10})
      })
    })

    describe('lerp', () => {
      it('should interpolate between two numbers', () => {
        expect(runtime('lerp', {a: 10, b: -10, alpha: 0}, {})).toEqual({
          out: 10,
        })
        expect(runtime('lerp', {a: 10, b: -10, alpha: 1}, {})).toEqual({
          out: -10,
        })
        expect(runtime('lerp', {a: 10, b: -10, alpha: 0.5}, {})).toEqual({
          out: 0,
        })
      })
    })
  })
})
