import {NodeType, NodeTypeImplementation} from '../schematic-core/entities'

export const nodeTypes: {[kind: string]: NodeType} = {
  '+': {
    id: '+',
    name: '+',
    fullName: 'Add',
    group: ['Math'],
    ports: {
      a: {id: 'a', kind: 'ValueInput', dataType: 'Num'},
      b: {id: 'b', kind: 'ValueInput', dataType: 'Num'},
      out: {id: 'out', kind: 'ValueOutput', dataType: 'Num'},
    },
    properties: {},
  },
  '-': {
    id: '-',
    name: '-',
    fullName: 'Subtract',
    group: ['Math'],
    ports: {
      a: {id: 'a', kind: 'ValueInput', dataType: 'Num'},
      b: {id: 'b', kind: 'ValueInput', dataType: 'Num'},
      out: {id: 'out', kind: 'ValueOutput', dataType: 'Num'},
    },
    properties: {},
  },
  '*': {
    id: '*',
    name: '*',
    fullName: 'Multiply',
    group: ['Math'],
    ports: {
      a: {id: 'a', kind: 'ValueInput', dataType: 'Num'},
      b: {id: 'b', kind: 'ValueInput', dataType: 'Num'},
      out: {id: 'out', kind: 'ValueOutput', dataType: 'Num'},
    },
    properties: {},
  },
  '/': {
    id: '/',
    name: '/',
    fullName: 'Divide',
    group: ['Math'],
    ports: {
      a: {id: 'a', kind: 'ValueInput', dataType: 'Num'},
      b: {id: 'b', kind: 'ValueInput', dataType: 'Num'},
      out: {id: 'out', kind: 'ValueOutput', dataType: 'Num'},
    },
    properties: {},
  },
  pow: {
    id: 'pow',
    name: 'pow',
    fullName: 'Power',
    group: ['Math'],
    ports: {
      a: {id: 'a', kind: 'ValueInput', dataType: 'Num'},
      b: {id: 'b', kind: 'ValueInput', dataType: 'Num'},
      out: {id: 'out', kind: 'ValueOutput', dataType: 'Num'},
    },
    properties: {},
  },

  max: {
    id: 'max',
    name: 'max',
    fullName: 'Max',
    group: ['Math'],
    ports: {
      a: {id: 'a', kind: 'ValueInput', dataType: 'Num'},
      b: {id: 'b', kind: 'ValueInput', dataType: 'Num'},
      out: {id: 'out', kind: 'ValueOutput', dataType: 'Num'},
    },
    properties: {},
  },
  min: {
    id: 'min',
    name: 'min',
    fullName: 'Min',
    group: ['Math'],
    ports: {
      a: {id: 'a', kind: 'ValueInput', dataType: 'Num'},
      b: {id: 'b', kind: 'ValueInput', dataType: 'Num'},
      out: {id: 'out', kind: 'ValueOutput', dataType: 'Num'},
    },
    properties: {},
  },
  abs: {
    id: 'abs',
    name: 'abs',
    fullName: 'Abs',
    group: ['Math'],
    ports: {
      in: {id: 'in', kind: 'ValueInput', dataType: 'Num'},
      out: {id: 'out', kind: 'ValueOutput', dataType: 'Num'},
    },
    properties: {},
  },
  lerp: {
    id: 'lerp',
    name: 'lerp',
    fullName: 'Lerp',
    group: ['Math'],
    ports: {
      a: {id: 'a', kind: 'ValueInput', dataType: 'Num'},
      b: {id: 'b', kind: 'ValueInput', dataType: 'Num'},
      alpha: {id: 'alpha', kind: 'ValueInput', dataType: 'Num'},
      out: {id: 'out', kind: 'ValueOutput', dataType: 'Num'},
    },
    properties: {},
  },
}

export const implementations: {[kind: string]: NodeTypeImplementation} = {
  '+': {
    id: '+',

    getPort(_, ctrl) {
      return ctrl.getPort('a') + ctrl.getPort('b')
    },
  },
  '-': {
    id: '-',

    getPort(_, ctrl) {
      return ctrl.getPort('a') - ctrl.getPort('b')
    },
  },
  '*': {
    id: '*',

    getPort(_, ctrl) {
      return ctrl.getPort('a') * ctrl.getPort('b')
    },
  },
  '/': {
    id: '/',

    getPort(_, ctrl) {
      return ctrl.getPort('a') / ctrl.getPort('b')
    },
  },
  pow: {
    id: 'pow',

    getPort(_, ctrl) {
      return ctrl.getPort('a') ** ctrl.getPort('b')
    },
  },

  max: {
    id: 'max',

    getPort(_, ctrl) {
      return Math.max(ctrl.getPort('a'), ctrl.getPort('b'))
    },
  },
  min: {
    id: 'min',

    getPort(_, ctrl) {
      return Math.min(ctrl.getPort('a'), ctrl.getPort('b'))
    },
  },
  abs: {
    id: 'abs',

    getPort(_, ctrl) {
      return Math.abs(ctrl.getPort('in'))
    },
  },
  lerp: {
    id: 'lerp',

    getPort(_, ctrl) {
      const a = ctrl.getPort('a')
      const b = ctrl.getPort('b')
      const alpha = Math.min(Math.max(ctrl.getPort('alpha'), 0), 1)

      return a * (1 - alpha) + b * alpha
    },
  },
}
